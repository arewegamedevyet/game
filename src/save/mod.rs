use amethyst::error::Error;
use amethyst::ecs::{SystemData, Join, Entity, Read, Write, ReadStorage, Entities};
use amethyst::utils::tag::Tag;
use serde::{Serialize, Deserialize};

use crate::prefab;
use crate::game::{CurrentRoom, GameTime};

pub use self::player::Player;

pub mod player;

pub trait Save<'s>: Sized + Serialize {
    type SystemData: SystemData<'s>;
    fn save(entity: Entity, system_data: Self::SystemData) -> Option<Self>;
}

pub trait Load<'s, 'de>: Default + Deserialize<'de> {
    type SystemData: SystemData<'s>;
    fn load(self, entity: Entity, system_data: Self::SystemData) -> Result<(), Error>;
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Global {
    room: String,
    time: u64,
    player: Player,
}

impl<'s> Save<'s> for Global {
    type SystemData = (
        Entities<'s>,
        Read<'s, CurrentRoom>,
        Read<'s, GameTime>,
        ReadStorage<'s, Tag<prefab::player::Player>>,
        <Player as Save<'s>>::SystemData,
    );

    fn save(_: Entity, (entities, current_room, game_time, players, player_data): Self::SystemData) -> Option<Self> {
        let player = (&entities, &players)
            .join()
            .next()
            .and_then(move |(e, _)| Player::save(e, player_data))?;
        Some(Global {
            room: current_room.room.clone(),
            time: game_time.time,
            player,
        })
    }
}

impl<'s, 'de> Load<'s, 'de> for Global {
    type SystemData = (
        Entities<'s>,
        Read<'s, CurrentRoom>,
        Write<'s, GameTime>,
        ReadStorage<'s, Tag<prefab::player::Player>>,
        <Player as Load<'s, 'de>>::SystemData,
    );

    fn load(
        self,
        _: Entity,
        (entities, current_room, mut game_time, players, (poses, mut switch_room)): Self::SystemData,
    ) -> Result<(), Error> {
        game_time.time = self.time;
        let player = self.player;
        switch_room.room = Some(self.room.clone());
        switch_room.init = true;
        switch_room.loading = false;
        (&entities, &players)
            .join()
            .next()
            .map(move |(e, _)| player.load(e, (poses, switch_room)))
            .expect("no player entity")?;
        Ok(())
    }
}
