use amethyst::error::Error;
use amethyst::ecs::{Entity, Write, ReadStorage, WriteStorage};
use amethyst_rhusics::rhusics_core::{Pose, NextFrame};
use amethyst_rhusics::rhusics_core::physics3d::BodyPose3;

use cgmath::Rotation3;

use crate::save::{Save, Load};
use crate::game::SwitchRoom;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Player {
    position: [f32; 3],
    rotation: f32,
}

impl<'s> Save<'s> for Player {
    type SystemData = ReadStorage<'s, BodyPose3<f32>>;

    fn save(entity: Entity, poses: Self::SystemData) -> Option<Self> {
        let pose = poses.get(entity)?;
        Some(Player {
            position: pose.position().into(),
            rotation: cgmath::Deg::from(cgmath::Euler::from(pose.rotation()).y).0,
        })
    }
}

impl<'s, 'de> Load<'s, 'de> for Player {
    type SystemData = (WriteStorage<'s, NextFrame<BodyPose3<f32>>>, Write<'s, SwitchRoom>);

    fn load(self, entity: Entity, (mut poses, mut switch_room): Self::SystemData) -> Result<(), Error> {
        let pose = BodyPose3::new(
            From::from(self.position),
            cgmath::Quaternion::from_angle_y(cgmath::Rad::from(cgmath::Deg(self.rotation))),
        );
        poses.insert(entity, NextFrame { value: pose })?;
        switch_room.isometry = Some((self.position, self.rotation));
        Ok(())
    }
}
