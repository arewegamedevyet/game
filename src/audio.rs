use std::vec;
use std::collections::HashMap;

use amethyst::ecs::prelude::*;
use amethyst::audio::{SourceHandle, Source, AudioEmitter, DecoderError};

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct Sound {
    sounds: HashMap<String, SourceHandle>,
}

impl Sound {
    pub fn new() -> Self {
        Sound { sounds: HashMap::new() }
    }

    pub fn get(&self, idx: &String) -> Option<SourceHandle> {
        self.sounds.get(idx).cloned()
    }

    pub fn insert(&mut self, idx: String, source: SourceHandle) {
        self.sounds.insert(idx, source);
    }
}

#[derive(Debug, Clone)]
pub struct Soundtrack {
    pub tracks: vec::IntoIter<SourceHandle>,
    mute: bool,
}

impl Soundtrack {
    pub fn new(mute: bool) -> Self {
        Soundtrack {
            tracks: vec![].into_iter(),
            mute,
        }
    }

    pub fn next(&mut self) -> Option<SourceHandle> {
        if self.mute {
            None
        } else {
            self.tracks.next()
        }
    }

    pub fn mute(&mut self, mute: bool) {
        self.mute = mute;
    }
}

#[derive(Debug, Default, Clone, Component)]
#[storage(VecStorage)]
pub struct SoundPlayer {
    pub sounds: HashMap<String, f32>,
}

impl SoundPlayer {
    pub fn play(
        &mut self,
        sound: String,
        timeout: f32,
        emitter: &mut AudioEmitter,
        source: &Source,
    ) -> Result<(), DecoderError> {
        if let Some(time) = self.sounds.get(&sound) {
            if *time > 0.0 {
                return Ok(());
            }
        }

        self.sounds.insert(sound, timeout);
        emitter.play(source)
    }

    pub fn update(&mut self, delta: f32) {
        for time in self.sounds.values_mut() {
            *time -= delta;
        }
    }
}
