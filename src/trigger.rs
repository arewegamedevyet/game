use std::vec;
use std::fmt::{self, Display};

use amethyst::ecs::Entity;

use crate::inventory::Item;

#[derive(Debug, Clone, PartialEq)]
pub enum Trigger {
    Update(f32),
    Random(f32),
    SaveGame,
    LoadGame,
    EnterRoom,
    LeaveRoom,
    Contact(Entity, Entity),
    LookAt(Entity, Entity),
    UnlockDoor(Entity),
    PickupItem(Entity, Item),
    Activate(Entity, Entity),
}

impl Trigger {
    pub fn kind(&self) -> TriggerKind {
        match self {
            Trigger::Update(..) => TriggerKind::Update,
            Trigger::Random(..) => TriggerKind::Random,
            Trigger::SaveGame => TriggerKind::SaveGame,
            Trigger::LoadGame => TriggerKind::LoadGame,
            Trigger::EnterRoom => TriggerKind::EnterRoom,
            Trigger::LeaveRoom => TriggerKind::LeaveRoom,
            Trigger::Contact(_, _) => TriggerKind::Contact,
            Trigger::LookAt(_, _) => TriggerKind::LookAt,
            Trigger::UnlockDoor(_) => TriggerKind::UnlockDoor,
            Trigger::PickupItem(_, _) => TriggerKind::PickupItem,
            Trigger::Activate(_, _) => TriggerKind::Activate,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TriggerKind {
    Update,
    Random,
    SaveGame,
    LoadGame,
    EnterRoom,
    LeaveRoom,
    Contact,
    LookAt,
    UnlockDoor,
    PickupItem,
    Activate,
}

impl Display for TriggerKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            TriggerKind::Update => write!(f, "update"),
            TriggerKind::Random => write!(f, "random"),
            TriggerKind::SaveGame => write!(f, "save-game"),
            TriggerKind::LoadGame => write!(f, "load-game"),
            TriggerKind::EnterRoom => write!(f, "enter-room"),
            TriggerKind::LeaveRoom => write!(f, "leave-room"),
            TriggerKind::Contact => write!(f, "contact"),
            TriggerKind::LookAt => write!(f, "look-at"),
            TriggerKind::UnlockDoor => write!(f, "unlock-door"),
            TriggerKind::PickupItem => write!(f, "pickup-item"),
            TriggerKind::Activate => write!(f, "activate"),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Iter {
    inner: vec::IntoIter<TriggerKind>,
}

impl Iter {
    pub fn new() -> Self {
        Default::default()
    }
}

impl Default for Iter {
    fn default() -> Self {
        Iter {
            inner: vec![
                TriggerKind::Update,
                TriggerKind::Random,
                TriggerKind::SaveGame,
                TriggerKind::LoadGame,
                TriggerKind::EnterRoom,
                TriggerKind::LeaveRoom,
                TriggerKind::Contact,
                TriggerKind::LookAt,
                TriggerKind::UnlockDoor,
                TriggerKind::PickupItem,
                TriggerKind::Activate,
            ]
            .into_iter(),
        }
    }
}

impl Iterator for Iter {
    type Item = TriggerKind;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}
