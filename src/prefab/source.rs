use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::{Loader, Handle, AssetStorage, ProgressCounter, AssetPrefab, PrefabData};
use amethyst::audio::{Source, WavFormat, OggFormat};

use crate::audio::Sound;
use crate::audio::Soundtrack;

#[derive(Clone, Default, Serialize, Deserialize)]
#[serde(default)]
pub struct Sources {
    sources: Vec<AbstractSource>,
}

#[derive(Clone, Serialize, Deserialize)]
pub enum AbstractSource {
    Sound(String, AssetPrefab<Source, WavFormat>),
    Music(AssetPrefab<Source, OggFormat>),
}

impl<'s> PrefabData<'s> for Sources {
    type Result = ();
    type SystemData = (
        WriteExpect<'s, Sound>,
        WriteExpect<'s, Soundtrack>,
        (
            ReadExpect<'s, Loader>,
            WriteStorage<'s, Handle<Source>>,
            Read<'s, AssetStorage<Source>>,
        ),
    );

    fn add_to_entity(
        &self,
        e: Entity,
        (sounds, st, system_data): &mut Self::SystemData,
        entities: &[Entity],
    ) -> Result<Self::Result, Error> {
        let mut tracks = vec![];
        for source in &self.sources {
            match source {
                AbstractSource::Sound(name, source) => {
                    source.add_to_entity(e, system_data, entities)?;
                    let handle = match source {
                        AssetPrefab::Handle(ref handle) => handle.clone(),
                        AssetPrefab::File(..) => unreachable!(),
                    };
                    sounds.insert(name.clone(), handle);
                }
                AbstractSource::Music(source) => {
                    source.add_to_entity(e, system_data, entities)?;
                    let handle = match source {
                        AssetPrefab::Handle(ref handle) => handle.clone(),
                        AssetPrefab::File(..) => unreachable!(),
                    };
                    tracks.push(handle);
                }
            }
        }
        st.tracks = tracks.into_iter();
        Ok(())
    }

    fn load_sub_assets(
        &mut self,
        progress: &mut ProgressCounter,
        (_, _, system_data): &mut Self::SystemData,
    ) -> Result<bool, Error> {
        let mut result = false;
        for source in &mut self.sources {
            match source {
                AbstractSource::Sound(_, source) => {
                    result = source.load_sub_assets(progress, system_data)? || result;
                }
                AbstractSource::Music(source) => {
                    result = source.load_sub_assets(progress, system_data)? || result;
                }
            }
        }
        Ok(result)
    }
}
