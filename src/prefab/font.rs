use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::{Loader, Handle, AssetPrefab, AssetStorage, ProgressCounter, PrefabData};
use amethyst::ui::{FontAsset, OtfFormat};

use crate::font::Font;

#[derive(Clone, Default, Deserialize, Serialize)]
pub struct Fonts {
    fonts: Vec<(String, AssetPrefab<FontAsset, OtfFormat>)>,
}

impl<'s> PrefabData<'s> for Fonts {
    type Result = ();
    type SystemData = (
        WriteExpect<'s, Font>,
        (
            ReadExpect<'s, Loader>,
            WriteStorage<'s, Handle<FontAsset>>,
            Read<'s, AssetStorage<FontAsset>>,
        ),
    );

    fn add_to_entity(&self, _: Entity, (fonts, _): &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        for (name, font) in &self.fonts {
            match font {
                AssetPrefab::Handle(handle) => fonts.insert(name.clone(), handle.clone()),
                AssetPrefab::File(..) => unreachable!(),
            }
        }
        Ok(())
    }

    fn load_sub_assets(
        &mut self,
        progress: &mut ProgressCounter,
        (_, system_data): &mut Self::SystemData,
    ) -> Result<bool, Error> {
        let mut result = false;
        for (_, font) in &mut self.fonts {
            result = font.load_sub_assets(progress, system_data)? || result;
        }
        Ok(result)
    }
}
