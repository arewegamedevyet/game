use amethyst::Error;
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::*;
use amethyst::utils::tag::Tag;
use amethyst::assets::PrefabData;

use nalgebra::Isometry3;

#[derive(Debug, Clone, Copy, Default, Component)]
#[storage(NullStorage)]
pub struct Camera;

#[derive(Debug, Clone, Copy, Component)]
pub struct FollowCamera {
    pub isometry: Isometry3<f32>,
}

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct ViewPrefab {
    pos: [f32; 3],
    rot: [f32; 2],
}

impl<'s> PrefabData<'s> for ViewPrefab {
    type Result = ();
    type SystemData = Read<'s, LazyUpdate>;

    fn add_to_entity(&self, entity: Entity, lazy: &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        let mut transform = Transform::default();
        transform.set_position(From::from(self.pos));
        let rot = self.rot;
        transform.set_rotation_euler(rot[0].to_radians(), rot[1].to_radians(), 0.);

        lazy.insert(entity, Tag::<Camera>::default());
        lazy.insert(
            entity,
            FollowCamera {
                isometry: transform.isometry().clone(),
            },
        );
        lazy.insert(entity, transform);

        Ok(())
    }
}
