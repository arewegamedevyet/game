use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::PrefabData;
use amethyst_rhusics::rhusics_core::{
    CollisionShape,
    CollisionStrategy,
    CollisionMode,
    Pose,
    PhysicalEntity,
    Material,
    NextFrame,
    physics3d::{Mass3, Velocity3, ForceAccumulator3},
    collide3d::BodyPose3
};
use amethyst_rhusics::collision::{Aabb3,
    primitive::{Primitive3, Particle3, Quad, Sphere, Cuboid, Cube, Cylinder, Capsule, ConvexPolyhedron}
};
use cgmath::Rotation3;

use crate::physics::ObjectType;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Primitive3Prefab {
    Particle,
    Quad(f32, f32),
    Sphere(f32),
    Cuboid(f32, f32, f32),
    Cube(f32),
    Cylinder(f32, f32),
    Capsule(f32, f32),
    ConvexPolyhedron(Vec<[f32; 3]>),
}

impl From<Primitive3Prefab> for Primitive3<f32> {
    fn from(prefab: Primitive3Prefab) -> Self {
        match prefab {
            Primitive3Prefab::Particle => Primitive3::Particle(Particle3::new()),
            Primitive3Prefab::Quad(x, y) => Primitive3::Quad(Quad::new(x, y)),
            Primitive3Prefab::Sphere(r) => Primitive3::Sphere(Sphere::new(r)),
            Primitive3Prefab::Cuboid(w, h, d) => Primitive3::Cuboid(Cuboid::new(w, h, d)),
            Primitive3Prefab::Cube(w) => Primitive3::Cube(Cube::new(w)),
            Primitive3Prefab::Cylinder(half_height, r) => Primitive3::Cylinder(Cylinder::new(half_height, r)),
            Primitive3Prefab::Capsule(half_height, r) => Primitive3::Capsule(Capsule::new(half_height, r)),
            Primitive3Prefab::ConvexPolyhedron(vec) => {
                Primitive3::ConvexPolyhedron(ConvexPolyhedron::new(vec.into_iter().map(From::from).collect()))
            }
        }
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ShapePrefab {
    Static {
        strategy: CollisionStrategy,
        mode: CollisionMode,
        primitive: Primitive3Prefab,
        #[serde(rename = "type")]
        ty: ObjectType,
        position: [f32; 3],
        rotation: f32,
        restitution: f32,
        density: f32,
        mass: Mass,
    },
    Dynamic {
        strategy: CollisionStrategy,
        mode: CollisionMode,
        primitive: Primitive3Prefab,
        #[serde(rename = "type")]
        ty: ObjectType,
        position: [f32; 3],
        rotation: f32,
        restitution: f32,
        density: f32,
        mass: Mass,
        velocity: [f32; 3],
    },
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub enum Mass {
    Mass(f32),
    Infinite,
}

impl From<Mass> for Mass3<f32> {
    fn from(mass: Mass) -> Self {
        match mass {
            Mass::Mass(mass) => Mass3::new(mass),
            Mass::Infinite => Mass3::infinite(),
        }
    }
}

impl<'s> PrefabData<'s> for ShapePrefab {
    type Result = ();
    type SystemData = Read<'s, LazyUpdate>;

    fn add_to_entity(&self, entity: Entity, lazy: &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        let (strategy, mode, primitive, ty, position, rotation, density, restitution, mass) = match self {
            ShapePrefab::Static {
                strategy,
                mode,
                primitive,
                ty,
                position,
                rotation,
                restitution,
                density,
                mass,
            }
            | ShapePrefab::Dynamic {
                strategy,
                mode,
                primitive,
                ty,
                position,
                rotation,
                restitution,
                density,
                mass,
                ..
            } => {
                (
                    strategy,
                    mode,
                    primitive,
                    ty,
                    position,
                    rotation,
                    restitution,
                    density,
                    mass,
                )
            }
        };
        lazy.insert(
            entity,
            CollisionShape::<Primitive3<f32>, BodyPose3<f32>, Aabb3<f32>, ObjectType>::new_simple_with_type(
                strategy.clone(),
                mode.clone(),
                From::from(primitive.clone()),
                ty.clone(),
            ),
        );
        lazy.insert(entity, ty.clone());
        lazy.insert(
            entity,
            BodyPose3::<f32>::new(
                cgmath::Point3::from(*position),
                cgmath::Quaternion::from_angle_y(cgmath::Rad::from(cgmath::Deg(*rotation))),
            ),
        );
        lazy.insert(
            entity,
            PhysicalEntity::<f32>::new(Material::new(*density, *restitution)),
        );
        lazy.insert(entity, Mass3::from(*mass));
        if let ShapePrefab::Dynamic {
            velocity,
            position,
            rotation,
            ..
        } = self
        {
            lazy.insert(entity, Velocity3::from_linear(cgmath::Vector3::from(*velocity)));
            lazy.insert(
                entity,
                NextFrame {
                    value: BodyPose3::<f32>::new(
                        cgmath::Point3::from(*position),
                        cgmath::Quaternion::from_angle_y(cgmath::Rad::from(cgmath::Deg(*rotation))),
                    ),
                },
            );
            lazy.insert(
                entity,
                NextFrame {
                    value: Velocity3::from_linear(cgmath::Vector3::from(*velocity)),
                },
            );
            lazy.insert(entity, ForceAccumulator3::<f32>::new());
        }
        Ok(())
    }
}
