use amethyst::Error;
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::*;
use amethyst::utils::tag::Tag;
use amethyst::assets::PrefabData;
use amethyst::audio::{AudioEmitter, AudioListener, output::Output};

use nalgebra::Point3;

use crate::physics::OnGround;
use crate::audio::SoundPlayer;

#[derive(Debug, Clone, Copy, Default, Component)]
#[storage(NullStorage)]
pub struct Player;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct PlayerPrefab;

impl<'s> PrefabData<'s> for PlayerPrefab {
    type Result = ();
    type SystemData = (Read<'s, LazyUpdate>, Option<Read<'s, Output>>);

    fn add_to_entity(
        &self,
        entity: Entity,
        (lazy, output): &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        lazy.insert(entity, Tag::<Player>::default());
        lazy.insert(entity, OnGround::default());
        lazy.insert(entity, Transform::default());
        lazy.insert(entity, SoundPlayer::default());
        lazy.insert(entity, AudioEmitter::new());
        if let Some(output) = output {
            lazy.insert(
                entity,
                AudioListener {
                    output: output.clone(),
                    left_ear: Point3::new(-0.1, 0., -0.1),
                    right_ear: Point3::new(0.1, 0., -0.1),
                },
            );
        }
        Ok(())
    }
}
