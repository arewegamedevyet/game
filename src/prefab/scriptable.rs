use std::path::Path;

use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::PrefabData;

use crate::trigger;
use crate::scripting::{AssociatedScript, ScriptRes};
use crate::roomstate::Name;

#[derive(Debug, Clone, Default, Deserialize, Serialize)]
pub struct ScriptPrefab;

impl<'s> PrefabData<'s> for ScriptPrefab {
    type Result = ();
    type SystemData = (ReadStorage<'s, Name>, Read<'s, LazyUpdate>, ReadExpect<'s, ScriptRes>);

    fn add_to_entity(
        &self,
        entity: Entity,
        (names, lazy, script_res): &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        let mut scripts = AssociatedScript::new();
        if let Some(name) = names.get(entity) {
            for trigger in trigger::Iter::new() {
                let path = format!("resources/scripts/{}/{}.lpi", name.name(), trigger);
                let path_ref: &Path = path.as_ref();
                if path_ref.exists() {
                    scripts.insert(&script_res, trigger, &format!("{}/{}.lpi", name.name(), trigger));
                }
            }
        }
        lazy.insert(entity, scripts);
        Ok(())
    }
}
