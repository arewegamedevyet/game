use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::PrefabData;
use amethyst_rhusics::rhusics_core::{
    CollisionShape,
    CollisionStrategy,
    CollisionMode,
    Pose,
    NextFrame,
    collide3d::BodyPose3
};
use amethyst_rhusics::collision::{Aabb3, primitive::{Primitive3, ConvexPolyhedron}};

use cgmath::One;

use crate::physics::{ObjectType, ViewPicker, PickerRef};

#[derive(Debug, Clone, Copy, Default, Deserialize, Serialize)]
pub struct ViewPickerPrefab(f32, f32, f32);

impl<'s> PrefabData<'s> for ViewPickerPrefab {
    type Result = ();
    type SystemData = Read<'s, LazyUpdate>;

    fn add_to_entity(&self, entity: Entity, lazy: &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        let ViewPickerPrefab(w, h, d) = *self;
        lazy.insert(entity, ViewPicker::default());
        let poly = ConvexPolyhedron::new(vec![
            cgmath::Point3::new(0., 0., 0.),
            cgmath::Point3::new(-w / 2., h / 2., d),
            cgmath::Point3::new(w / 2., h / 2., d),
            cgmath::Point3::new(-w / 2., -h / 2., d),
            cgmath::Point3::new(w / 2., -h / 2., d),
        ]);
        lazy.insert(
            entity,
            CollisionShape::<Primitive3<f32>, BodyPose3<f32>, Aabb3<f32>, ObjectType>::new_simple_with_type(
                CollisionStrategy::FullResolution,
                CollisionMode::Discrete,
                Primitive3::ConvexPolyhedron(poly),
                ObjectType::Fov,
            ),
        );
        let pose = BodyPose3::<f32>::new(cgmath::Point3::new(0., 0., 0.), cgmath::Quaternion::one());
        lazy.insert(entity, NextFrame { value: pose.clone() });
        lazy.insert(entity, pose);
        Ok(())
    }
}

#[derive(Debug, Clone, Copy, Default, Deserialize, Serialize)]
pub struct PickerRefPrefab(usize);

impl<'s> PrefabData<'s> for PickerRefPrefab {
    type Result = ();
    type SystemData = WriteStorage<'s, PickerRef>;

    fn add_to_entity(
        &self,
        this: Entity,
        refs: &mut Self::SystemData,
        entities: &[Entity],
    ) -> Result<Self::Result, Error> {
        let PickerRefPrefab(entity) = *self;
        refs.insert(this, PickerRef(entities[entity]))?;
        Ok(())
    }
}
