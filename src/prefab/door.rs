use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::PrefabData;

use crate::inventory::KeyColor;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DoorPrefab {
    /// Time interval/destination list
    pub list: DestinationList,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum TimeCycle {
    RealTime,
    GameTime,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct DestinationList {
    /// Time cycle, in-game or real-time
    pub time_cycle: TimeCycle,
    /// Length of a cycle.
    pub modulo: u64,
    /// Seconds per destination.
    pub destinations: Vec<Destination>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum DestType {
    Inner,
    Outer(String),
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Destination {
    /// Destination type: either inner (within the same room) or outer (in a different room).
    pub ty: DestType,
    /// Duration of the opening in seconds.
    pub duration: u64,
    /// Position in next room.
    pub position: [f32; 3],
    /// Rotation after stepping through the door.
    pub rotation: f32,
}

#[derive(Clone, Debug, Component)]
#[storage(HashMapStorage)]
pub struct NextRoom {
    /// Time interval/destination list
    pub list: DestinationList,
}

impl<'s> PrefabData<'s> for DoorPrefab {
    type Result = ();
    type SystemData = Read<'s, LazyUpdate>;

    fn add_to_entity(&self, entity: Entity, lazy: &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        lazy.insert(
            entity,
            NextRoom {
                list: self.list.clone(),
            },
        );
        Ok(())
    }
}

#[derive(Clone, Debug, Component)]
#[storage(HashMapStorage)]
pub struct Locked {
    pub color: KeyColor,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct LockedPrefab {
    color: KeyColor,
}

impl<'s> PrefabData<'s> for LockedPrefab {
    type Result = ();
    type SystemData = Read<'s, LazyUpdate>;

    fn add_to_entity(&self, entity: Entity, lazy: &mut Self::SystemData, _: &[Entity]) -> Result<Self::Result, Error> {
        lazy.insert(entity, Locked { color: self.color });
        Ok(())
    }
}
