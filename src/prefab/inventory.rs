use amethyst::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::{
    PrefabData,
};

use crate::inventory::{Inventory, Item};

#[derive(Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct InventoryPrefab {
    inventory: Inventory,
}

impl<'s> PrefabData<'s> for InventoryPrefab {
    type Result = ();
    type SystemData = WriteStorage<'s, Inventory>;

    fn add_to_entity(
        &self,
        this: Entity,
        inventories: &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        inventories.insert(this, self.inventory.clone())?;
        Ok(())
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ItemPrefab {
    item: Item,
}

impl<'s> PrefabData<'s> for ItemPrefab {
    type Result = ();
    type SystemData = WriteStorage<'s, Item>;

    fn add_to_entity(
        &self,
        this: Entity,
        inventories: &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        inventories.insert(this, self.item.clone())?;
        Ok(())
    }
}
