use std::collections::HashMap;

use amethyst::ui::FontHandle;

#[derive(Debug, Default, Clone)]
pub struct Font {
    fonts: HashMap<String, FontHandle>,
}

impl Font {
    pub fn get(&self, idx: &String) -> Option<FontHandle> {
        self.fonts.get(idx).cloned()
    }

    pub fn insert(&mut self, idx: String, font: FontHandle) {
        self.fonts.insert(idx, font);
    }
}
