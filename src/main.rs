#![allow(clippy::type_complexity)]
#![feature(try_from)]
#[macro_use]
extern crate amethyst;
#[macro_use]
extern crate specs_derive;
#[macro_use]
extern crate serde;
extern crate nalgebra as na;

use amethyst::{
    StateEventReader,
    prelude::*,
    renderer::{DisplayConfig, DrawSkybox, DrawToonSeparate, Pipeline, RenderBundle, Stage},
    animation::VertexSkinningBundle,
    utils::application_dir,
    input::InputBundle,
    core::transform::{Transform, TransformBundle},
    animation::AnimationBundle,
    assets::{PrefabLoaderSystem},
    config::Config,
    audio::{AudioBundle, AudioSystem, DjSystem},
    ui::{DrawUi, UiBundle},
    shrev::EventChannel,
};
use amethyst_gltf::GltfSceneLoaderSystem;
use amethyst_rhusics::DefaultPhysicsBundle3;

use crate::init::Init;
use crate::game::{Consts, GameDataBuilder, GameState, SwitchRoom, CurrentRoom, GameTime, Delete};
use crate::prefab::ScenePrefabData;
use crate::audio::{Soundtrack, Sound};
use crate::physics::ObjectType;
use crate::trigger::Trigger;
use crate::scripting::{ScriptRes, msg::Message};

pub mod init;
pub mod loading;
pub mod save;
pub mod game;
pub mod prefab;
pub mod audio;
pub mod physics;
pub mod systems;
pub mod font;
pub mod inventory;
pub mod roomstate;
pub mod trigger;
pub mod scripting;

const CONFIG: &str = "resources/config.ron";

/// Global configuration.
#[derive(Debug, Serialize, Deserialize)]
pub struct GlobalConfig {
    /// Input resource path.
    pub input_config: String,
    /// Input resource path.
    pub display_config: String,
    /// Constants path.
    pub consts: String,
    /// Init-prefab path.
    pub init: String,
    /// Mute in-game music.
    pub mute_music: bool,
}

impl Default for GlobalConfig {
    fn default() -> Self {
        GlobalConfig {
            input_config: "resources/input_config.ron".to_string(),
            display_config: "resources/display_config.ron".to_string(),
            consts: "resources/consts.ron".to_string(),
            init: "init.ron".to_string(),
            mute_music: false,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SaveFile {
    directory: String,
    file: String,
}

impl SaveFile {
    pub fn directory(&self) -> &str {
        &self.directory
    }

    pub fn full_path(&self) -> String {
        format!("{}/{}", self.directory, self.file)
    }
}

impl Default for SaveFile {
    fn default() -> Self {
        SaveFile {
            directory: "resources/savedata/autosave".to_string(),
            file: "save.ron".to_string(),
        }
    }
}

/// Program entry point
///
/// Creates the `Application`, `GameData`, initialize bundles.
fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let config = GlobalConfig::load(application_dir(CONFIG)?);

    let display_config = application_dir(&config.display_config)?;
    let display_config = DisplayConfig::load(&display_config);

    let consts = application_dir(&config.consts)?;
    let consts = Consts::load(&consts);

    let input_config = application_dir(&config.input_config)?;
    let input_bundle = InputBundle::<String, String>::new().with_bindings_from_file(input_config)?;

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawSkybox::new())
            .with_pass(DrawToonSeparate::new())
            .with_pass(DrawUi::new()),
    );

    let game_data = GameDataBuilder::new()
        .with_core(PrefabLoaderSystem::<ScenePrefabData>::default(), "scene_loader", &[])
        .with_core(
            GltfSceneLoaderSystem::default(),
            "gltf_loader",
            &["scene_loader"], // This is important so that entity instantiation is performed in a single frame.
        )
        .with_core_bundle(RenderBundle::new(pipe, Some(display_config)))?
        .with_core_bundle(
            AnimationBundle::<usize, Transform>::new("animation_control", "sampler_interpolation")
                .with_dep(&["gltf_loader"]),
        )?
        .with_core_bundle(TransformBundle::new().with_dep(&["animation_control", "sampler_interpolation"]))?
        .with_core_bundle(VertexSkinningBundle::new().with_dep(&[
            "transform_system",
            "animation_control",
            "sampler_interpolation",
        ]))?
        .with_core_bundle(input_bundle)?
        .with_core_bundle(UiBundle::<String, String>::new())?
        .with_core_bundle(AudioBundle)?
        .with_core(AudioSystem::new(), "audio_system", &[])
        .with_core(DjSystem::new(|st: &mut Soundtrack| st.next()), "dj_system", &[])
        .with_core(systems::SoundPlayerSystem::default(), "sound_player_system", &[])
        .with_fixed_bundle(
            GameState::Gameplay,
            DefaultPhysicsBundle3::<ObjectType>::new().with_spatial(),
        )?
        .with_fixed(
            GameState::Gameplay,
            systems::ViewPickerSystem::default(),
            "view_picker_system",
            &[],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::RhusicsParentSystem::default(),
            "rhusics_parent_system",
            &[],
        )
        .with_fixed(GameState::Gameplay, systems::NewRoomSystem, "newroom_loader", &[])
        .with_fixed(
            GameState::Gameplay,
            systems::MovementControlSystem::default(),
            "movement_controller",
            &[],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::ActivationControlSystem::default(),
            "activation_controller",
            &[],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::AnimationControlSystem::default(),
            "animation_controller",
            &[],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::SoundControlSystem::default(),
            "sound_controller",
            &[],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::FollowCameraSystem::default(),
            "follow_camera_system",
            &[],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::OnGroundSystem::default(),
            "on_ground_system",
            &[],
        )
        .with_fixed(GameState::Gameplay, systems::SaveSystem::default(), "save_system", &[])
        .with_fixed(GameState::Gameplay, systems::LoadSystem::default(), "load_system", &[])
        .with_fixed(
            GameState::Gameplay,
            systems::RoomStateSaveSystem::default(),
            "roomstate_save_system",
            &["activation_controller"],
        )
        .with_fixed(
            GameState::Gameplay,
            systems::RoomStateLoadSystem::default(),
            "roomstate_load_system",
            &[],
        )
        .with(
            GameState::Gameplay,
            systems::ContactSystem::default(),
            "contact_system",
            &[],
        )
        .with(
            GameState::Gameplay,
            systems::ScriptSystem::default(),
            "script_system",
            &["contact_system"],
        )
        .with(
            GameState::Gameplay,
            systems::EvalMessageSystem::default(),
            "eval_message_system",
            &["script_system"],
        )
        .with(
            GameState::Gameplay,
            systems::GameTargetSystem::default(),
            "game_target_system",
            &["eval_message_system"],
        )
        .with(
            GameState::Gameplay,
            systems::RoomTargetSystem::default(),
            "room_target_system",
            &["eval_message_system"],
        )
        .with(
            GameState::Gameplay,
            systems::EntityTargetSystem::default(),
            "entity_target_system",
            &["eval_message_system"],
        );

    let init_path = config.init.clone();

    let assets_path = application_dir(r"assets/")?;
    let mut game = ApplicationBuilder::<_, _, _, StateEventReader>::new(assets_path, Init::new(init_path))?
        .with_resource(Sound::new())
        .with_resource(Soundtrack::new(config.mute_music))
        .with_resource(config)
        .with_resource(consts)
        .with_resource(SaveFile::default())
        .with_resource(SwitchRoom::default())
        .with_resource(CurrentRoom::default())
        .with_resource(GameTime::default())
        .with_resource(EventChannel::<Trigger>::new())
        .with_resource(Delete::default())
        .with_resource(ScriptRes::new())
        .with_resource(EventChannel::<Message>::new())
        .build(game_data)?;

    game.run();

    Ok(())
}
