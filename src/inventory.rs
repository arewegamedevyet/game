use std::str::FromStr;

use amethyst::ecs::prelude::*;

#[derive(Debug, Default, Clone, Component, Serialize, Deserialize)]
#[storage(HashMapStorage)]
pub struct Inventory {
    pub items: Vec<Item>,
}

impl Inventory {
    pub fn add(&mut self, item: Item) {
        self.items.push(item);
    }

    pub fn take(&mut self, idx: usize) -> Option<Item> {
        if idx < self.items.len() {
            Some(self.items.remove(idx))
        } else {
            None
        }
    }

    pub fn contains(&self, item: &Item) -> Option<usize> {
        self.items.iter().position(|e| e == item)
    }

    pub fn contains_by_name(&self, item: &str) -> Option<usize> {
        self.items.iter().position(|e| e.name() == item)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct InvalidItem;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, Component)]
pub enum Item {
    Key(KeyColor),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum KeyColor {
    Silver,
}

impl Item {
    pub fn name(&self) -> &str {
        match self {
            Item::Key(KeyColor::Silver) => "key-silver",
        }
    }
}

impl FromStr for Item {
    type Err = InvalidItem;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "key-silver" => Ok(Item::Key(KeyColor::Silver)),
            _ => Err(InvalidItem),
        }
    }
}
