use std::fmt::{self, Display};
use std::collections::HashMap;

use amethyst::error::Error;
use amethyst::ecs::prelude::*;
use amethyst::assets::PrefabData;

use crate::inventory::{Item, KeyColor};
use crate::prefab::door::Locked;

#[derive(Debug, Default)]
pub struct NamesRes {
    map: HashMap<String, usize>,
}

impl NamesRes {
    pub fn new<S: ToString>(&mut self, name: S) -> Name {
        let name = name.to_string();
        let idx_mut = self.map.entry(name.clone()).or_insert(0);
        let idx = *idx_mut;
        *idx_mut += 1;
        Name { name, idx }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Component, Serialize, Deserialize)]
#[storage(VecStorage)]
pub struct Name {
    name: String,
    idx: usize,
}

impl Name {
    pub fn new<S: AsRef<str>>(name: S, idx: usize) -> Self {
        Name {
            name: name.as_ref().to_string(),
            idx,
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn idx(&self) -> usize {
        self.idx
    }
}

impl Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{{{}}}", self.name, self.idx)
    }
}

#[derive(Debug, Default, Clone, PartialEq, Serialize, Deserialize)]
pub struct EntityState {
    pub exists: Option<bool>,
    pub locked: Option<KeyColor>,
    pub item: Option<Item>,
}

impl<'s> PrefabData<'s> for EntityState {
    type Result = ();
    type SystemData = (Read<'s, LazyUpdate>, Entities<'s>);

    fn add_to_entity(
        &self,
        this: Entity,
        (lazy, entities): &mut Self::SystemData,
        _: &[Entity],
    ) -> Result<Self::Result, Error> {
        if let Some(false) = self.exists {
            entities.delete(this)?;
            return Ok(());
        }
        if let Some(color) = self.locked {
            lazy.insert(this, Locked { color });
        } else {
            lazy.remove::<Locked>(this);
        }
        if let Some(ref item) = self.item {
            lazy.insert(this, item.clone());
        }
        Ok(())
    }
}
