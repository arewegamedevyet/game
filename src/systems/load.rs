use std::marker::PhantomData;

use amethyst::config::Config;
use amethyst::input::InputHandler;
use amethyst::ecs::{Join, System, Read, Write, ReadStorage, Entities};
use amethyst::shrev::EventChannel;
use amethyst::utils::tag::Tag;

use crate::SaveFile;
use crate::save::{self, Load};
use crate::prefab::room::Room;
use crate::trigger::Trigger;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct LoadSystem<'de>(PhantomData<&'de ()>);

impl<'s, 'de> System<'s> for LoadSystem<'de> {
    type SystemData = (
        ReadStorage<'s, Tag<Room>>,
        Write<'s, EventChannel<Trigger>>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, SaveFile>,
        Entities<'s>,
        <save::Global as Load<'s, 'de>>::SystemData,
    );

    fn run(&mut self, (rooms, mut triggers, input, save_file, entities, load_global_system_data): Self::SystemData) {
        let quick_load = input.action_is_down("quick-load").unwrap_or_default();

        if quick_load {
            let room = (&rooms, &entities).join().next().map(|(_, e)| e).unwrap();
            if let Ok(global) = <save::Global as Config>::load_no_fallback(&save_file.full_path()) {
                global
                    .load(room, load_global_system_data)
                    .expect("couldn't load save file");
                triggers.single_write(Trigger::LoadGame);
            }
        }
    }
}
