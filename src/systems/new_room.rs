use amethyst::ecs::{Join, System, Write, ReadStorage, WriteStorage};
use amethyst::utils::tag::Tag;
use amethyst_rhusics::rhusics_core::{NextFrame, Pose};
use amethyst_rhusics::rhusics_core::physics3d::BodyPose3;

use cgmath::Rotation3;

use crate::game::SwitchRoom;
use crate::prefab::player::Player;

/// Does one-time initialization when a room finishes loading.
#[derive(Debug, Default)]
pub struct NewRoomSystem;

impl<'s> System<'s> for NewRoomSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        WriteStorage<'s, NextFrame<BodyPose3<f32>>>,
        Write<'s, SwitchRoom>,
    );

    fn run(&mut self, (players, mut next_poses, mut next_room): Self::SystemData) {
        if let SwitchRoom {
            isometry: ref mut iso @ Some(_),
            loading: false,
            ..
        } = *next_room
        {
            for (_, next_pose) in (&players, &mut next_poses).join() {
                let (position, rotation) = iso.take().unwrap();
                next_pose.value.set_position(From::from(position));
                next_pose
                    .value
                    .set_rotation(cgmath::Quaternion::from_angle_y(cgmath::Deg(rotation)));
            }
        }
    }
}
