use std::fs;

use amethyst::config::Config;
use amethyst::ecs::{Resources, Join, System, Read, ReadStorage, Entities};
use amethyst::shrev::{EventChannel, ReaderId};
use amethyst::utils::{application_dir, tag::Tag};
use amethyst::assets::PrefabData;

use crate::SaveFile;
use crate::game::{CurrentRoom, Delete};
use crate::roomstate::{Name, EntityState};
use crate::inventory::Item;
use crate::prefab::door::Locked;
use crate::trigger::Trigger;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct RoomStateSaveSystem {
    trigger_reader: Option<ReaderId<Trigger>>,
}

impl<'s> System<'s> for RoomStateSaveSystem {
    type SystemData = (
        ReadStorage<'s, Name>,
        ReadStorage<'s, Tag<EntityState>>,
        ReadStorage<'s, Locked>,
        ReadStorage<'s, Item>,
        Read<'s, EventChannel<Trigger>>,
        Read<'s, SaveFile>,
        Read<'s, CurrentRoom>,
        Read<'s, Delete>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut triggers = res.fetch_mut::<EventChannel<Trigger>>();
        self.trigger_reader = Some(triggers.register_reader());
    }

    fn run(
        &mut self,
        (names, tags, locks, items, triggers, save_file, current_room, delete, entities): Self::SystemData,
    ) {
        let save = triggers
            .read(self.trigger_reader.as_mut().unwrap())
            .any(|trigger| &Trigger::LeaveRoom == trigger || &Trigger::SaveGame == trigger);
        if save {
            for (name, _, entity) in (&names, &tags, &entities).join() {
                let mut state = EntityState::default();
                state.exists = Some(true);
                if let Some(lock) = locks.get(entity) {
                    state.locked = Some(lock.color);
                }
                if let Some(item) = items.get(entity) {
                    state.item = Some(item.clone());
                }
                let dir = application_dir(format!("{}/{}", save_file.directory(), current_room.room))
                    .expect("couldn't get savefile directory");
                fs::create_dir_all(dir).expect("couldn't create savefile directory");
                let path = format!("{}/{}/{}.ron", save_file.directory(), current_room.room, name);
                state.write(path).expect("couldn't save entity state");
            }
            for name in delete.deleted() {
                let mut state = EntityState::default();
                state.exists = Some(false);
                let dir = application_dir(format!("{}/{}", save_file.directory(), current_room.room))
                    .expect("couldn't get savefile directory");
                fs::create_dir_all(dir).expect("couldn't create savefile directory");
                let path = format!("{}/{}/{}.ron", save_file.directory(), current_room.room, name);
                state.write(path).expect("couldn't save entity state");
            }
        }
    }
}

/// Simple player controller.
#[derive(Debug, Default)]
pub struct RoomStateLoadSystem {
    trigger_reader: Option<ReaderId<Trigger>>,
}

impl<'s> System<'s> for RoomStateLoadSystem {
    type SystemData = (
        ReadStorage<'s, Name>,
        ReadStorage<'s, Tag<EntityState>>,
        Read<'s, EventChannel<Trigger>>,
        Read<'s, SaveFile>,
        Read<'s, CurrentRoom>,
        Entities<'s>,
        <EntityState as PrefabData<'s>>::SystemData,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut triggers = res.fetch_mut::<EventChannel<Trigger>>();
        self.trigger_reader = Some(triggers.register_reader());
    }

    fn run(&mut self, (names, tags, triggers, save_file, current_room, entities, mut sd): Self::SystemData) {
        let load = triggers
            .read(self.trigger_reader.as_mut().unwrap())
            .any(|trigger| &Trigger::EnterRoom == trigger || &Trigger::LoadGame == trigger);
        if load {
            for (name, _, entity) in (&names, &tags, &entities).join() {
                let path = format!("{}/{}/{}.ron", save_file.directory(), current_room.room, name);
                if let Ok(state) = EntityState::load_no_fallback(path) {
                    state.add_to_entity(entity, &mut sd, &[]).expect("couldn't load entity");
                }
            }
        }
    }
}
