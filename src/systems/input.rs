use std::f32;
use std::time::{SystemTime, UNIX_EPOCH};

use amethyst::core::Transform;
use amethyst::input::InputHandler;
use amethyst::ecs::{Resources, Join, System, Read, Write, ReadStorage, WriteStorage, Entities};
use amethyst::shrev::{EventChannel, ReaderId};
use amethyst::utils::tag::Tag;
use amethyst::animation::{get_animation_set, AnimationSet, AnimationControlSet, EndControl, AnimationCommand};
use amethyst::assets::AssetStorage;
use amethyst::audio::{Source, AudioEmitter};
use amethyst_rhusics::rhusics_core::{NextFrame, Pose};
use amethyst_rhusics::rhusics_core::physics3d::{BodyPose3, ForceAccumulator3};

use cgmath::Rotation3;

use crate::game::{Consts, SwitchRoom, GameTime, Delete};
use crate::physics::{ViewPicker, PickerRef, OnGround};
use crate::audio::{Sound, SoundPlayer};
use crate::prefab::player::Player;
use crate::prefab::door::{NextRoom, TimeCycle, DestType, Locked};
use crate::inventory::{Inventory, Item};
use crate::roomstate::Name;
use crate::trigger::Trigger;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct MovementControlSystem;

impl<'s> System<'s> for MovementControlSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        WriteStorage<'s, ForceAccumulator3<f32>>,
        WriteStorage<'s, NextFrame<BodyPose3<f32>>>,
        WriteStorage<'s, OnGround>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, Consts>,
    );

    fn run(&mut self, (players, mut forces, mut next_poses, mut on_ground, input, consts): Self::SystemData) {
        let vertical = input.axis_value("vertical").unwrap_or_default() as f32;
        let horizontal = input.axis_value("horizontal").unwrap_or_default() as f32;
        let jump = input.action_is_down("jump").unwrap_or_default();

        for (_, force, on_ground) in (&players, &mut forces, &mut on_ground).join() {
            if jump && on_ground.on_ground {
                force.add_force(cgmath::Vector3::new(0., 5. * consts.move_accel, 0.));
                on_ground.on_ground = false;
            }
        }

        for (_, force, next_pose) in (&players, &mut forces, &mut next_poses).join() {
            let accel = cgmath::Vector3::new(consts.move_accel * horizontal, 0., consts.move_accel * vertical);
            force.add_force(accel);
            let rot = horizontal.atan2(vertical);
            if vertical.abs() > f32::EPSILON || horizontal.abs() > f32::EPSILON {
                next_pose
                    .value
                    .set_rotation(cgmath::Quaternion::from_angle_y(cgmath::Rad(rot)));
            }
        }
    }
}

/// Simple player controller.
#[derive(Debug, Default)]
pub struct ActivationControlSystem {
    trigger_reader: Option<ReaderId<Trigger>>,
}

impl<'s> System<'s> for ActivationControlSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        WriteStorage<'s, NextFrame<BodyPose3<f32>>>,
        ReadStorage<'s, ViewPicker>,
        ReadStorage<'s, PickerRef>,
        ReadStorage<'s, NextRoom>,
        WriteStorage<'s, Inventory>,
        WriteStorage<'s, Locked>,
        ReadStorage<'s, Item>,
        ReadStorage<'s, Name>,
        Write<'s, EventChannel<Trigger>>,
        Read<'s, InputHandler<String, String>>,
        Write<'s, SwitchRoom>,
        Read<'s, GameTime>,
        Write<'s, Delete>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut triggers = res.fetch_mut::<EventChannel<Trigger>>();
        self.trigger_reader = Some(triggers.register_reader());
    }

    fn run(
        &mut self,
        (
            players,
            mut next_poses,
            pickers,
            picker_refs,
            next_room,
            mut inventories,
            mut locks,
            items,
            names,
            mut triggers,
            input,
            mut switch,
            game_time,
            mut delete,
            entities,
        ): Self::SystemData,
    ) {
        let activate = input.action_is_down("activate").unwrap_or_default();

        if activate {
            for (_, inventory, next_pose, picker_ref, player) in
                (&players, &mut inventories, &mut next_poses, &picker_refs, &entities).join()
            {
                if let Some(picker) = pickers.get(picker_ref.0) {
                    if let (Some(entity), _) = (picker.entity, picker.point) {
                        if let Some(item) = items.get(entity) {
                            // grab item
                            inventory.add(item.clone());
                            if let Some(name) = names.get(entity) {
                                delete
                                    .delete_named(entity, name.clone(), &entities)
                                    .expect("couldn't delete item");
                            } else {
                                entities.delete(entity).expect("couldn't delete item");
                            }
                            triggers.single_write(Trigger::PickupItem(player, item.clone()));
                        } else if let Some(lock) = locks.get(entity) {
                            // unlock door
                            if let Some(position) = inventory.contains(&Item::Key(lock.color)) {
                                inventory.take(position);
                                locks.remove(entity);
                                triggers.single_write(Trigger::UnlockDoor(entity));
                            }
                        } else if let Some(next) = next_room.get(entity) {
                            // walk through door
                            let mut time = match next.list.time_cycle {
                                TimeCycle::RealTime => {
                                    SystemTime::now()
                                        .duration_since(UNIX_EPOCH)
                                        .expect("did you go back in time?!")
                                        .as_secs()
                                }
                                TimeCycle::GameTime => game_time.time,
                            };
                            time %= next.list.modulo;
                            for dest in &next.list.destinations {
                                if time < dest.duration {
                                    match dest.ty {
                                        DestType::Inner => {
                                            next_pose.value.set_position(cgmath::Point3::from(dest.position));
                                            next_pose.value.set_rotation(cgmath::Quaternion::from_angle_y(
                                                cgmath::Rad::from(cgmath::Deg(dest.rotation)),
                                            ));
                                        }
                                        DestType::Outer(ref room) => {
                                            triggers.single_write(Trigger::LeaveRoom);
                                            switch.room = Some(room.clone());
                                            switch.isometry = Some((dest.position, dest.rotation));
                                            switch.init = true;
                                            switch.loading = false;
                                        }
                                    }
                                    break;
                                } else {
                                    time -= dest.duration;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/// Simple player controller.
#[derive(Debug, Default)]
pub struct AnimationControlSystem;

impl<'s> System<'s> for AnimationControlSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        WriteStorage<'s, AnimationControlSet<usize, Transform>>,
        ReadStorage<'s, AnimationSet<usize, Transform>>,
        Read<'s, InputHandler<String, String>>,
        Entities<'s>,
    );

    fn run(&mut self, (players, mut anim_controls, anim_sets, input, entities): Self::SystemData) {
        let vertical = input.axis_value("vertical").unwrap_or_default() as f32;
        let horizontal = input.axis_value("horizontal").unwrap_or_default() as f32;

        for (_, e, sets) in (&players, &entities, &anim_sets).join() {
            const PLAYER_NEUTRAL: usize = 0;
            const PLAYER_WALKING_LOWER: usize = 1;
            const PLAYER_WALKING_UPPER: usize = 2;
            if let Some(anim) = get_animation_set(&mut anim_controls, e) {
                if vertical.abs() > f32::EPSILON || horizontal.abs() > f32::EPSILON {
                    if anim.has_animation(PLAYER_NEUTRAL) {
                        anim.abort(PLAYER_NEUTRAL);
                    }
                    let lower = sets.get(&PLAYER_WALKING_LOWER).unwrap();
                    let upper = sets.get(&PLAYER_WALKING_UPPER).unwrap();
                    if !anim.has_animation(PLAYER_WALKING_LOWER) {
                        anim.add_animation(
                            PLAYER_WALKING_LOWER,
                            lower,
                            EndControl::Loop(None),
                            1.0,
                            AnimationCommand::Start,
                        );
                    } else {
                        anim.start(PLAYER_WALKING_LOWER);
                    }
                    if !anim.has_animation(PLAYER_WALKING_UPPER) {
                        anim.add_animation(
                            PLAYER_WALKING_UPPER,
                            upper,
                            EndControl::Loop(None),
                            1.0,
                            AnimationCommand::Start,
                        );
                    } else {
                        anim.start(PLAYER_WALKING_UPPER);
                    }
                } else {
                    if anim.has_animation(PLAYER_WALKING_LOWER) {
                        anim.abort(PLAYER_WALKING_LOWER);
                    }
                    if anim.has_animation(PLAYER_WALKING_UPPER) {
                        anim.abort(PLAYER_WALKING_UPPER);
                    }
                    let neutral = sets.get(&PLAYER_NEUTRAL).unwrap();
                    if !anim.has_animation(PLAYER_NEUTRAL) {
                        anim.add_animation(PLAYER_NEUTRAL, neutral, EndControl::Stay, 1.0, AnimationCommand::Start);
                    } else {
                        anim.start(PLAYER_NEUTRAL);
                    }
                }
            }
        }
    }
}

/// Simple player controller.
#[derive(Debug, Default)]
pub struct SoundControlSystem;

impl<'s> System<'s> for SoundControlSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Player>>,
        WriteStorage<'s, AudioEmitter>,
        WriteStorage<'s, SoundPlayer>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, AssetStorage<Source>>,
        Read<'s, Sound>,
    );

    fn run(&mut self, (players, mut emitters, mut sound_players, input, sound_storage, sounds): Self::SystemData) {
        let vertical = input.axis_value("vertical").unwrap_or_default() as f32;
        let horizontal = input.axis_value("horizontal").unwrap_or_default() as f32;

        for (_, emitter, sound_player) in (&players, &mut emitters, &mut sound_players).join() {
            if vertical.abs() > f32::EPSILON || horizontal.abs() > f32::EPSILON {
                let sound_name = "player-step".to_string();
                if let Some(sound) = sounds.get(&sound_name) {
                    if let Some(sound) = sound_storage.get(&sound) {
                        sound_player
                            .play(sound_name, 0.75, emitter, sound)
                            .expect("couldn't play sound");
                    }
                }
            }
        }
    }
}
