use amethyst::core::Time;
use amethyst::ecs::{Join, System, Read, WriteStorage};

use crate::audio::SoundPlayer;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct SoundPlayerSystem;

impl<'s> System<'s> for SoundPlayerSystem {
    type SystemData = (WriteStorage<'s, SoundPlayer>, Read<'s, Time>);

    fn run(&mut self, (mut players, time): Self::SystemData) {
        for player in (&mut players).join() {
            player.update(time.delta_seconds())
        }
    }
}
