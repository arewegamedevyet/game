use std::fs;

use amethyst::config::Config;
use amethyst::input::InputHandler;
use amethyst::ecs::{Join, System, Read, Write, ReadStorage, Entities};
use amethyst::shrev::EventChannel;
use amethyst::utils::{application_dir, tag::Tag};

use crate::SaveFile;
use crate::save::{self, Save};
use crate::prefab::room::Room;
use crate::trigger::Trigger;

/// Simple player controller.
#[derive(Debug, Default)]
pub struct SaveSystem;

impl<'s> System<'s> for SaveSystem {
    type SystemData = (
        ReadStorage<'s, Tag<Room>>,
        Write<'s, EventChannel<Trigger>>,
        Read<'s, InputHandler<String, String>>,
        Read<'s, SaveFile>,
        Entities<'s>,
        <save::Global as Save<'s>>::SystemData,
    );

    fn run(&mut self, (rooms, mut triggers, input, save_file, entities, save_global_system_data): Self::SystemData) {
        let quick_save = input.action_is_down("quick-save").unwrap_or_default();

        if quick_save {
            let room = (&rooms, &entities).join().next().map(|(_, e)| e).unwrap();
            let dir = application_dir(save_file.directory()).expect("couldn't get savefile directory");
            fs::create_dir_all(dir).expect("couldn't create savefile directory");
            let save = save::Global::save(room, save_global_system_data).expect("couldn't save game state");;
            save.write(&save_file.full_path()).expect("couldn't write to save file");
            triggers.single_write(Trigger::SaveGame);
        }
    }
}
