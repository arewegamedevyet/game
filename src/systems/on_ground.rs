use std::f32;

use amethyst::ecs::{Resources, System, Read, WriteStorage};
use amethyst::shrev::{ReaderId, EventChannel};
use amethyst_rhusics::rhusics_ecs::collide3d::ContactEvent3;

use crate::physics::OnGround;

fn pointing_up(normal: &cgmath::Vector3<f32>) -> bool {
    normal.y > 60.0_f32.to_radians().sin()
}

fn pointing_down(normal: &cgmath::Vector3<f32>) -> bool {
    -normal.y > 60.0_f32.to_radians().sin()
}

/// Simple player controller.
#[derive(Debug, Default)]
pub struct OnGroundSystem {
    reader: Option<ReaderId<ContactEvent3<f32>>>,
}

impl<'s> System<'s> for OnGroundSystem {
    type SystemData = (WriteStorage<'s, OnGround>, Read<'s, EventChannel<ContactEvent3<f32>>>);

    fn setup(&mut self, res: &mut Resources) {
        let mut events = res
            .entry::<EventChannel<ContactEvent3<f32>>>()
            .or_insert_with(Default::default);
        self.reader = Some(events.register_reader());
    }

    fn run(&mut self, (mut on_ground, contact_events): Self::SystemData) {
        for contact in contact_events.read(self.reader.as_mut().unwrap()) {
            let (entity_a, entity_b) = contact.bodies;
            if pointing_down(&contact.contact.normal) {
                if let Some(on_ground) = on_ground.get_mut(entity_a) {
                    on_ground.on_ground = true;
                }
            }
            if pointing_up(&contact.contact.normal) {
                if let Some(on_ground) = on_ground.get_mut(entity_b) {
                    on_ground.on_ground = true;
                }
            }
        }
    }
}
