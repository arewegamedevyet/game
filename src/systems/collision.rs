use amethyst::ecs::{Join, Resources, System, Read, Write, ReadStorage, Entities};
use amethyst::shrev::{ReaderId, EventChannel};
use amethyst_rhusics::rhusics_ecs::collide3d::ContactEvent3;

use crate::trigger::Trigger;
use crate::physics::{ObjectType, RhusicsParent};

#[derive(Debug, Default)]
pub struct ContactSystem {
    reader: Option<ReaderId<ContactEvent3<f32>>>,
}

impl<'s> System<'s> for ContactSystem {
    type SystemData = (
        ReadStorage<'s, RhusicsParent>,
        ReadStorage<'s, ObjectType>,
        Write<'s, EventChannel<Trigger>>,
        Read<'s, EventChannel<ContactEvent3<f32>>>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut events = res
            .entry::<EventChannel<ContactEvent3<f32>>>()
            .or_insert_with(Default::default);
        self.reader = Some(events.register_reader());
    }

    fn run(&mut self, (parents, types, mut triggers, contact_events, entities): Self::SystemData) {
        let contacts = contact_events.read(self.reader.as_mut().unwrap()).collect::<Vec<_>>();
        for (e, parent, a_ty) in (&entities, &parents, &types).join() {
            for contact in &contacts {
                if contact.bodies.0 != parent.entity
                    && contact.bodies.1 != parent.entity
                    && (contact.bodies.0 == e || contact.bodies.1 == e)
                {
                    if contact.bodies.0 == e {
                        if let Some(b_ty) = types.get(contact.bodies.1) {
                            match (a_ty, b_ty) {
                                (ObjectType::Dynamic, ObjectType::Dynamic)
                                | (ObjectType::Dynamic, ObjectType::Player)
                                | (ObjectType::Player, ObjectType::Dynamic) => {
                                    triggers.single_write(Trigger::Contact(e, contact.bodies.1))
                                }
                                _ => {}
                            }
                        }
                    } else {
                        if let Some(b_ty) = types.get(contact.bodies.0) {
                            match (a_ty, b_ty) {
                                (ObjectType::Dynamic, ObjectType::Dynamic)
                                | (ObjectType::Dynamic, ObjectType::Player)
                                | (ObjectType::Player, ObjectType::Dynamic) => {
                                    triggers.single_write(Trigger::Contact(e, contact.bodies.0))
                                }
                                _ => {}
                            }
                        }
                    }
                }
            }
        }
        for (e, _, a_ty) in (&entities, !&parents, &types).join() {
            for contact in &contacts {
                if contact.bodies.0 == e || contact.bodies.1 == e {
                    if contact.bodies.0 == e {
                        if let Some(b_ty) = types.get(contact.bodies.1) {
                            match (a_ty, b_ty) {
                                (ObjectType::Dynamic, ObjectType::Dynamic)
                                | (ObjectType::Dynamic, ObjectType::Player)
                                | (ObjectType::Player, ObjectType::Dynamic) => {
                                    triggers.single_write(Trigger::Contact(e, contact.bodies.1))
                                }
                                _ => {}
                            }
                        }
                    } else {
                        if let Some(b_ty) = types.get(contact.bodies.0) {
                            match (a_ty, b_ty) {
                                (ObjectType::Dynamic, ObjectType::Dynamic)
                                | (ObjectType::Dynamic, ObjectType::Player)
                                | (ObjectType::Player, ObjectType::Dynamic) => {
                                    triggers.single_write(Trigger::Contact(e, contact.bodies.0))
                                }
                                _ => {}
                            }
                        }
                    }
                }
            }
        }
    }
}
