use amethyst::core::Transform;
use amethyst::ecs::{Join, Resources, System, Read, ReadExpect, Write, ReadStorage, WriteStorage, Entities};
use amethyst::shrev::{EventChannel, ReaderId};
use amethyst_rhusics::rhusics_core::{Pose, NextFrame, collide3d::BodyPose3, physics3d::ForceAccumulator3};

use nalgebra::Vector3;

use crate::game::{CurrentRoom, SwitchRoom};
use crate::inventory::Inventory;
use crate::trigger::Trigger;
use crate::roomstate::Name;
use crate::scripting::{AssociatedScript, ScriptRes};
use crate::scripting::raw::{self, Eval};
use crate::scripting::msg::{Message, Target, Command};

/// Runs scripts on specific triggers.
#[derive(Debug, Default)]
pub struct ScriptSystem {
    trigger_reader: Option<ReaderId<Trigger>>,
}

impl<'s> System<'s> for ScriptSystem {
    type SystemData = (
        ReadStorage<'s, Name>,
        ReadStorage<'s, AssociatedScript>,
        Write<'s, EventChannel<Trigger>>,
        ReadExpect<'s, ScriptRes>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut triggers = res.fetch_mut::<EventChannel<Trigger>>();
        self.trigger_reader = Some(triggers.register_reader());
    }

    fn run(&mut self, (names, associated, triggers, scripts, entities): Self::SystemData) {
        for trigger in triggers.read(self.trigger_reader.as_mut().unwrap()) {
            scripts.trigger(&names, &associated, &entities, trigger);
        }
    }
}

/// Evaluates raw messages into usable messages.
#[derive(Debug, Default)]
pub struct EvalMessageSystem {
    msg_reader: Option<ReaderId<raw::Message>>,
}

impl<'s> System<'s> for EvalMessageSystem {
    type SystemData = (
        Write<'s, EventChannel<Message>>,
        ReadStorage<'s, Name>,
        ReadStorage<'s, Inventory>,
        ReadStorage<'s, BodyPose3<f32>>,
        ReadStorage<'s, Transform>,
        ReadExpect<'s, ScriptRes>,
        Read<'s, CurrentRoom>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let scripts = res.fetch::<ScriptRes>();
        self.msg_reader = Some(
            scripts
                .messages()
                .lock()
                .expect("message queue was poisoned")
                .register_reader(),
        );
    }

    fn run(
        &mut self,
        (mut messages, names, inventories, poses, transforms, scripts, current_room, entities): Self::SystemData,
    ) {
        let raw_messages = scripts.messages().lock().expect("message queue was poisoned");
        for msg in raw_messages.read(self.msg_reader.as_mut().unwrap()) {
            let msg = msg
                .eval((
                    &names,
                    &inventories,
                    &poses,
                    &transforms,
                    &scripts,
                    &current_room,
                    &entities,
                ))
                .expect("invalid message");
            if let Some(msg) = msg {
                messages.single_write(msg);
            }
        }
    }
}

/// Processes messages targeted at a room.
#[derive(Debug, Default)]
pub struct GameTargetSystem {
    msg_reader: Option<ReaderId<Message>>,
}

impl<'s> System<'s> for GameTargetSystem {
    type SystemData = (
        Read<'s, EventChannel<Message>>,
        Read<'s, CurrentRoom>,
        Write<'s, SwitchRoom>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut messages = res.fetch_mut::<EventChannel<Message>>();
        self.msg_reader = Some(messages.register_reader());
    }

    fn run(&mut self, (messages, _current_room, mut switch_room): Self::SystemData) {
        for msg in messages.read(self.msg_reader.as_mut().unwrap()) {
            match msg.target() {
                Target::Nil => {}
                Target::Game => {
                    match msg.cmd() {
                        Command::LoadRoom(room) => {
                            switch_room.init = true;
                            switch_room.room = Some(room.clone());
                        }
                        _ => {}
                    }
                }
                Target::Room(..) => {}
                Target::Entity(..) => {}
                Target::Entities(..) => {}
            }
        }
    }
}

/// Processes messages targeted at a room.
#[derive(Debug, Default)]
pub struct RoomTargetSystem {
    msg_reader: Option<ReaderId<Message>>,
}

impl<'s> System<'s> for RoomTargetSystem {
    type SystemData = (
        Read<'s, EventChannel<Message>>,
        Read<'s, CurrentRoom>,
        Write<'s, SwitchRoom>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut messages = res.fetch_mut::<EventChannel<Message>>();
        self.msg_reader = Some(messages.register_reader());
    }

    fn run(&mut self, (messages, _current_room, mut _switch_room): Self::SystemData) {
        for msg in messages.read(self.msg_reader.as_mut().unwrap()) {
            match msg.target() {
                Target::Nil => {}
                Target::Game => {}
                Target::Room(..) => {}
                Target::Entity(..) => {}
                Target::Entities(..) => {}
            }
        }
    }
}

/// Processes messages targeted at a room.
#[derive(Debug, Default)]
pub struct EntityTargetSystem {
    msg_reader: Option<ReaderId<Message>>,
}

impl<'s> System<'s> for EntityTargetSystem {
    type SystemData = (
        ReadStorage<'s, Name>,
        ReadStorage<'s, BodyPose3<f32>>,
        WriteStorage<'s, NextFrame<BodyPose3<f32>>>,
        WriteStorage<'s, ForceAccumulator3<f32>>,
        WriteStorage<'s, Transform>,
        WriteStorage<'s, Inventory>,
        Read<'s, EventChannel<Message>>,
        Entities<'s>,
    );

    fn setup(&mut self, res: &mut Resources) {
        let mut messages = res.fetch_mut::<EventChannel<Message>>();
        self.msg_reader = Some(messages.register_reader());
    }

    fn run(
        &mut self,
        (
            names,
            poses,
            mut next_poses,
            mut forces,
            mut transforms,
            mut inventories,
            messages,
            entities
        ): Self::SystemData,
    ) {
        for msg in messages.read(self.msg_reader.as_mut().unwrap()) {
            match msg.target() {
                Target::Nil => {}
                Target::Game => {}
                Target::Room(..) => {}
                Target::Entity(tname, tidx) => {
                    match msg.cmd() {
                        Command::DeleteEntity => {
                            for (name, e) in (&names, &entities).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    entities.delete(e).expect("couldn't delete entity");
                                }
                            }
                        }
                        Command::AddForce(x, y, z) => {
                            for (name, force) in (&names, &mut forces).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    force.add_force(cgmath::Vector3::from((*x, *y, *z)));
                                }
                            }
                        }
                        Command::TranslateBy(x, y, z) => {
                            for (name, pose, next_pose) in (&names, &poses, &mut next_poses).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    next_pose
                                        .value
                                        .set_position(pose.position() + cgmath::Vector3::from((*x, *y, *z)));
                                }
                            }
                        }
                        Command::RotateBy(x, y, z, w) => {
                            for (name, pose, next_pose) in (&names, &poses, &mut next_poses).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    next_pose
                                        .value
                                        .set_rotation(pose.rotation() + cgmath::Quaternion::from((*x, *y, *z, *w)));
                                }
                            }
                        }
                        Command::ScaleBy(x, y, z) => {
                            for (name, transform) in (&names, &mut transforms).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    let scale = *transform.scale();
                                    *transform.scale_mut() = scale.component_mul(&Vector3::from([*x, *y, *z]));
                                }
                            }
                        }
                        Command::SetPosition(x, y, z) => {
                            for (name, next_pose) in (&names, &mut next_poses).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    next_pose.value.set_position(cgmath::Point3::new(*x, *y, *z));
                                }
                            }
                        }
                        Command::SetRotation(x, y, z, w) => {
                            for (name, next_pose) in (&names, &mut next_poses).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    next_pose.value.set_rotation(cgmath::Quaternion::from((*x, *y, *z, *w)));
                                }
                            }
                        }
                        Command::SetScale(x, y, z) => {
                            for (name, transform) in (&names, &mut transforms).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    *transform.scale_mut() = Vector3::from([*x, *y, *z]);
                                }
                            }
                        }
                        Command::AddItem(item) => {
                            for (name, inventory) in (&names, &mut inventories).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    let item = item.parse().expect("invalid item");
                                    inventory.add(item);
                                }
                            }
                        }
                        Command::RemoveItem(item) => {
                            for (name, inventory) in (&names, &mut inventories).join() {
                                if name.name() == tname && name.idx() == *tidx {
                                    if let Some(idx) = inventory.contains_by_name(item) {
                                        inventory.take(idx);
                                    }
                                }
                            }
                        }
                        _ => {}
                    }
                }
                Target::Entities(tname) => {
                    match msg.cmd() {
                        Command::DeleteEntity => {
                            for (name, e) in (&names, &entities).join() {
                                if name.name() == tname {
                                    entities.delete(e).expect("couldn't delete entity");
                                }
                            }
                        }
                        Command::TranslateBy(x, y, z) => {
                            for (name, pose, next_pose) in (&names, &poses, &mut next_poses).join() {
                                if name.name() == tname {
                                    next_pose
                                        .value
                                        .set_position(pose.position() + cgmath::Vector3::from((*x, *y, *z)));
                                }
                            }
                        }
                        Command::RotateBy(x, y, z, w) => {
                            for (name, pose, next_pose) in (&names, &poses, &mut next_poses).join() {
                                if name.name() == tname {
                                    next_pose
                                        .value
                                        .set_rotation(pose.rotation() + cgmath::Quaternion::from((*x, *y, *z, *w)));
                                }
                            }
                        }
                        Command::ScaleBy(x, y, z) => {
                            for (name, transform) in (&names, &mut transforms).join() {
                                if name.name() == tname {
                                    let scale = *transform.scale();
                                    *transform.scale_mut() = scale.component_mul(&Vector3::from([*x, *y, *z]));
                                }
                            }
                        }
                        Command::SetPosition(x, y, z) => {
                            for (name, next_pose) in (&names, &mut next_poses).join() {
                                if name.name() == tname {
                                    next_pose.value.set_position(cgmath::Point3::new(*x, *y, *z));
                                }
                            }
                        }
                        Command::SetRotation(x, y, z, w) => {
                            for (name, next_pose) in (&names, &mut next_poses).join() {
                                if name.name() == tname {
                                    next_pose.value.set_rotation(cgmath::Quaternion::from((*x, *y, *z, *w)));
                                }
                            }
                        }
                        Command::SetScale(x, y, z) => {
                            for (name, transform) in (&names, &mut transforms).join() {
                                if name.name() == tname {
                                    *transform.scale_mut() = Vector3::from([*x, *y, *z]);
                                }
                            }
                        }
                        Command::AddItem(item) => {
                            for (name, inventory) in (&names, &mut inventories).join() {
                                if name.name() == tname {
                                    let item = item.parse().expect("invalid item");
                                    inventory.add(item);
                                }
                            }
                        }
                        Command::RemoveItem(item) => {
                            for (name, inventory) in (&names, &mut inventories).join() {
                                if name.name() == tname {
                                    if let Some(idx) = inventory.contains_by_name(item) {
                                        inventory.take(idx);
                                    }
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }
        }
    }
}
