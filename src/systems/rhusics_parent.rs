use amethyst::ecs::prelude::*;
use amethyst_rhusics::rhusics_core::{Pose, NextFrame};
use amethyst_rhusics::rhusics_ecs::physics3d::BodyPose3;

use crate::physics::RhusicsParent;

#[derive(Debug, Default)]
pub struct RhusicsParentSystem;

impl<'a> System<'a> for RhusicsParentSystem {
    type SystemData = (
        ReadStorage<'a, BodyPose3<f32>>,
        WriteStorage<'a, NextFrame<BodyPose3<f32>>>,
        ReadStorage<'a, RhusicsParent>,
    );

    fn run(&mut self, (poses, mut next_poses, parents): Self::SystemData) {
        for (next_pose, parent) in (&mut next_poses, &parents).join() {
            if let Some(pose) = poses.get(parent.entity) {
                let position = pose.position() + parent.offset;
                let rotation = pose.rotation() * parent.rotation;
                next_pose.value.set_position(position);
                next_pose.value.set_rotation(rotation);
            }
        }
    }
}
