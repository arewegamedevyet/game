#[derive(Debug, Clone, PartialEq)]
pub struct Message {
    target: Target,
    cmd: Command,
}

impl Message {
    pub fn new(target: Target, cmd: Command) -> Self {
        Message { target, cmd }
    }

    pub fn target(&self) -> &Target {
        &self.target
    }

    pub fn cmd(&self) -> &Command {
        &self.cmd
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Target {
    Nil,
    Game,
    Room(String),
    Entities(String),
    Entity(String, usize),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Command {
    Quit(Option<i32>, Option<String>),
    LoadRoom(String),
    Save(String),
    Load(String),
    CreateEntity(String),
    DeleteEntity,
    AddForce(f32, f32, f32),
    TranslateBy(f32, f32, f32),
    RotateBy(f32, f32, f32, f32),
    ScaleBy(f32, f32, f32),
    SetPosition(f32, f32, f32),
    SetRotation(f32, f32, f32, f32),
    SetScale(f32, f32, f32),
    AddItem(String),
    RemoveItem(String),
}
