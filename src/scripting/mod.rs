use std::fs;
use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::any::Any;

use amethyst::utils::application_dir;
use amethyst::ecs::prelude::*;
use amethyst::shrev::EventChannel;

use lispi::Lispi;
use lispi::expr::Value;
use lispi::compile::Program;

use crate::trigger::{Trigger, TriggerKind};
use crate::roomstate::Name;
use self::raw::Expr;

pub use msg::*;

pub mod raw;
pub mod msg;
pub mod ffi;

#[derive(Debug, Default, Clone, Component)]
#[storage(HashMapStorage)]
pub struct AssociatedScript {
    scripts: HashMap<TriggerKind, Program>,
}

impl AssociatedScript {
    pub fn new() -> Self {
        Default::default()
    }

    pub fn insert<P: AsRef<str>>(&mut self, scripts: &ScriptRes, trigger: TriggerKind, path: P) {
        let full_path = application_dir(&format!("resources/scripts/{}", path.as_ref())).expect("");
        let src = fs::read_to_string(full_path)
            .unwrap_or_else(|err| panic!("couldn't read src: resources/scripts/{}: {}", path.as_ref(), err));
        let mut interp = scripts.interp.lock().expect("lispi was poisoned");
        let program = interp
            .compile(Some(path.as_ref()), &src)
            .unwrap_or_else(|err| panic!("couldn't compile script: {}", err));
        self.scripts.insert(trigger, program);
    }
}

pub struct ScriptRes {
    interp: Mutex<Lispi>,
    scripts: HashMap<TriggerKind, Vec<Program>>,
    rx: Arc<Mutex<EventChannel<raw::Message>>>,
}

impl ScriptRes {
    pub fn new() -> Self {
        let mut interp = Lispi::default();
        interp.load_std().expect("couldn't load lispi libstd");
        interp.register_ffi(ffi::Game).unwrap_or_else(|err| panic!("{}", err));
        interp.register_ffi(ffi::Room).unwrap_or_else(|err| panic!("{}", err));
        interp
            .register_ffi(ffi::FindEntity)
            .unwrap_or_else(|err| panic!("{}", err));
        interp
            .register_ffi(ffi::FindEntities)
            .unwrap_or_else(|err| panic!("{}", err));
        let send_message = ffi::SendMessage::new();
        let rx = send_message.channel();
        interp
            .register_ffi(send_message)
            .unwrap_or_else(|err| panic!("{}", err));
        interp
            .register_ffi(ffi::Message)
            .unwrap_or_else(|err| panic!("{}", err));
        let scripts = HashMap::new();
        ScriptRes {
            interp: Mutex::new(interp),
            scripts,
            rx,
        }
    }

    pub fn messages(&self) -> &Mutex<EventChannel<raw::Message>> {
        &self.rx
    }

    pub fn interp(&self) -> &Mutex<Lispi> {
        &self.interp
    }

    pub fn register<P>(&mut self, trigger: TriggerKind, path: P)
    where
        P: AsRef<str>,
    {
        let full_path = application_dir(&format!("resources/scripts/{}", path.as_ref())).expect("");
        let src = fs::read_to_string(full_path)
            .unwrap_or_else(|err| panic!("couldn't read src: resources/scripts/{}: {}", path.as_ref(), err));
        let mut interp = self.interp.lock().expect("lispi was poisoned");
        let program = interp
            .compile(Some(path.as_ref()), &src)
            .unwrap_or_else(|err| panic!("couldn't compile script: {}", err));
        self.scripts.entry(trigger).or_default().push(program);
    }

    pub fn trigger<'s>(
        &self,
        names: &ReadStorage<'s, Name>,
        associated: &ReadStorage<'s, AssociatedScript>,
        entities_res: &Entities<'s>,
        trigger: &Trigger,
    ) {
        let mut interp = self.interp.lock().expect("lispi was poisoned");
        let kind = trigger.kind();
        if let Some(scripts) = self.scripts.get(&kind) {
            let args = match trigger {
                Trigger::Update(dt) => vec![Value::from(*dt as f64)],
                Trigger::Random(dt) => vec![Value::from(*dt as f64)],
                Trigger::SaveGame => vec![],
                Trigger::LoadGame => vec![],
                Trigger::EnterRoom => vec![],
                Trigger::LeaveRoom => vec![],
                Trigger::Contact(a, b) => {
                    let a = names.get(*a).expect("no name found for entity");
                    let a = Expr::from(a);
                    let a = Arc::new(a) as Arc<dyn Any + Send + Sync>;
                    let b = names.get(*b).expect("no name found for entity");
                    let b = Expr::from(b);
                    let b = Arc::new(b) as Arc<dyn Any + Send + Sync>;
                    vec![Value::from(a), Value::from(b)]
                }
                Trigger::LookAt(a, b) => {
                    let a = names.get(*a).expect("no name found for entity");
                    let a = Expr::from(a);
                    let a = Arc::new(a) as Arc<dyn Any + Send + Sync>;
                    let b = names.get(*b).expect("no name found for entity");
                    let b = Expr::from(b);
                    let b = Arc::new(b) as Arc<dyn Any + Send + Sync>;
                    vec![Value::from(a), Value::from(b)]
                }
                Trigger::UnlockDoor(door) => {
                    let door = names.get(*door).expect("no name found for entity");
                    let door = Expr::from(door);
                    let door = Arc::new(door) as Arc<dyn Any + Send + Sync>;
                    vec![Value::from(door)]
                }
                Trigger::PickupItem(entity, item) => {
                    let entity = names.get(*entity).expect("no name found for entity");
                    let entity = Expr::from(entity);
                    let entity = Arc::new(entity) as Arc<dyn Any + Send + Sync>;
                    vec![Value::from(entity), Value::from(item.name().to_string())]
                }
                Trigger::Activate(a, b) => {
                    let a = names.get(*a).expect("no name found for entity");
                    let a = Expr::from(a);
                    let a = Arc::new(a) as Arc<dyn Any + Send + Sync>;
                    let b = names.get(*b).expect("no name found for entity");
                    let b = Expr::from(b);
                    let b = Arc::new(b) as Arc<dyn Any + Send + Sync>;
                    vec![Value::from(a), Value::from(b)]
                }
            };
            for program in scripts {
                interp.load(program).unwrap_or_else(|err| panic!("{}", err));
                interp.call(kind.to_string(), &args).expect("couldn't execute trigger");
                interp.unload().expect("couldn't unload program");
            }
        }
        let (mut args, entities) = match trigger {
            Trigger::Update(..) => (vec![], vec![]),
            Trigger::Random(..) => (vec![], vec![]),
            Trigger::SaveGame | Trigger::LoadGame | Trigger::EnterRoom | Trigger::LeaveRoom => (vec![], vec![]),
            Trigger::Contact(a, b) => {
                let a_name = names.get(*a).expect("no name found for entity");
                let a_name = Expr::from(a_name);
                let a_name = Arc::new(a_name) as Arc<dyn Any + Send + Sync>;
                let b_name = names.get(*b).expect("no name found for entity");
                let b_name = Expr::from(b_name);
                let b_name = Arc::new(b_name) as Arc<dyn Any + Send + Sync>;
                (vec![Value::from(a_name), Value::from(b_name)], vec![a, b])
            }
            Trigger::LookAt(a, b) => {
                let a_name = names.get(*a).expect("no name found for entity");
                let a_name = Expr::from(a_name);
                let a_name = Arc::new(a_name) as Arc<dyn Any + Send + Sync>;
                let b_name = names.get(*b).expect("no name found for entity");
                let b_name = Expr::from(b_name);
                let b_name = Arc::new(b_name) as Arc<dyn Any + Send + Sync>;
                (vec![Value::from(a_name), Value::from(b_name)], vec![a, b])
            }
            Trigger::UnlockDoor(door) => {
                let door_name = names.get(*door).expect("no name found for entity");
                let door_name = Expr::from(door_name);
                let door_name = Arc::new(door_name) as Arc<dyn Any + Send + Sync>;
                (vec![Value::from(door_name)], vec![door])
            }
            Trigger::PickupItem(entity, item) => {
                let name = names.get(*entity).expect("no name found for entity");
                let name = Expr::from(name);
                let name = Arc::new(name) as Arc<dyn Any + Send + Sync>;
                let item_name = item.name().to_string();
                (vec![Value::from(name), Value::from(item_name)], vec![entity])
            }
            Trigger::Activate(a, b) => {
                let a_name = names.get(*a).expect("no name found for entity");
                let a_name = Expr::from(a_name);
                let a_name = Arc::new(a_name) as Arc<dyn Any + Send + Sync>;
                let b_name = names.get(*b).expect("no name found for entity");
                let b_name = Expr::from(b_name);
                let b_name = Arc::new(b_name) as Arc<dyn Any + Send + Sync>;
                (vec![Value::from(a_name), Value::from(b_name)], vec![a, b])
            }
        };
        for entity in entities {
            if let Some(associated) = associated.get(*entity) {
                if let Some(program) = associated.scripts.get(&kind) {
                    interp.load(program).unwrap_or_else(|err| panic!("{}", err));
                    interp.call(kind.to_string(), &args).expect("couldn't execute trigger");
                    interp.unload().expect("couldn't unload program");
                }
            }
            args.reverse();
        }
        for entity in entities_res.join() {
            if let Some(associated) = associated.get(entity) {
                if let Trigger::Update(dt) = trigger {
                    if let Some(program) = associated.scripts.get(&TriggerKind::Update) {
                        let args = vec![Value::from(*dt as f64)];
                        interp.load(program).unwrap_or_else(|err| panic!("{}", err));
                        interp.call(kind.to_string(), &args).expect("couldn't execute trigger");
                        interp.unload().expect("couldn't unload program");
                    }
                }
                if let Trigger::Random(dt) = trigger {
                    if let Some(program) = associated.scripts.get(&TriggerKind::Random) {
                        let args = vec![Value::from(*dt as f64)];
                        interp.load(program).unwrap_or_else(|err| panic!("{}", err));
                        interp.call(kind.to_string(), &args).expect("couldn't execute trigger");
                        interp.unload().expect("couldn't unload program");
                    }
                }
            }
        }
    }
}
