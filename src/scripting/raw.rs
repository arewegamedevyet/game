use std::sync::Arc;
use std::any::Any;

use amethyst::core::Transform;
use amethyst::ecs::prelude::*;
use amethyst_rhusics::rhusics_core::{Pose, collide3d::BodyPose3};

use lispi::collections::{LinkedList, RcStr};
use lispi::expr::{Value, ValueKind, Lambda};

use crate::game::CurrentRoom;
use crate::roomstate::Name;
use crate::inventory::Inventory;
use crate::scripting::{msg, ScriptRes};

#[derive(Debug, Clone, PartialEq)]
pub struct Message {
    pub target: Expr,
    pub cmd: Command,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Target {
    Game,
    Room(Expr),
    Entities(Expr),
    Entity(Expr, Expr),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Command {
    Nil,
    Bind(Expr, Arc<Lambda>),
    BindList(Vec<Expr>, Arc<Lambda>),
    Quit(Expr, Expr),
    LoadRoom(Expr),
    Save(Expr),
    Load(Expr),
    CreateEntity(Expr),
    DeleteEntity,
    AddForce(Expr, Expr, Expr),
    TranslateBy(Expr, Expr, Expr),
    RotateBy(Expr, Expr, Expr, Expr),
    ScaleBy(Expr, Expr, Expr),
    SetPosition(Expr, Expr, Expr),
    SetRotation(Expr, Expr, Expr, Expr),
    SetScale(Expr, Expr, Expr),
    AddItem(Expr),
    RemoveItem(Expr),
    CurrentRoom,
    IsKind(Expr),
    IsAlive,
    HasItem(Expr),
    PositionX,
    PositionY,
    PositionZ,
    RotationX,
    RotationY,
    RotationZ,
    RotationW,
    ScaleX,
    ScaleY,
    ScaleZ,
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expr {
    Nil,
    Atom(String),
    String(String),
    Bool(bool),
    Int(i64),
    Real(f32),
    Query(Arc<Message>),
    Target(Arc<Target>),
}

impl Default for Expr {
    fn default() -> Self {
        Expr::Nil
    }
}

impl<'a> From<&'a Name> for Expr {
    fn from(name: &'a Name) -> Self {
        Expr::Target(Arc::new(Target::Entity(
            Expr::String(name.name().to_string()),
            Expr::Int(name.idx() as _),
        )))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Error {
    InvalidType,
    InvalidTarget,
    InvalidEntity,
}

pub type SystemData<'a, 's> = (
    &'a ReadStorage<'s, Name>,
    &'a ReadStorage<'s, Inventory>,
    &'a ReadStorage<'s, BodyPose3<f32>>,
    &'a ReadStorage<'s, Transform>,
    &'a ReadExpect<'s, ScriptRes>,
    &'a Read<'s, CurrentRoom>,
    &'a Entities<'s>,
);

pub trait Eval<T> {
    fn eval(&self, system_data: SystemData) -> Result<T, Error>;
}

impl Eval<Value> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<Value, Error> {
        match self {
            Expr::Nil => Ok(Value::from(LinkedList::new())),
            Expr::Atom(atom) => Ok(Value::no_span(ValueKind::Atom(From::from(atom.clone())))),
            Expr::String(string) => Ok(Value::from(string.clone())),
            Expr::Bool(p) => Ok(Value::from(*p)),
            Expr::Int(i) => Ok(Value::from(*i)),
            Expr::Real(f) => Ok(Value::from(*f as f64)),
            Expr::Query(msg) => msg.eval(system_data),
            Expr::Target(target) => Ok(Value::from(Arc::clone(target) as Arc<dyn Any + Send + Sync>)),
        }
    }
}

impl<T> Eval<Option<T>> for Expr
where
    Expr: Eval<T>,
{
    fn eval(&self, system_data: SystemData) -> Result<Option<T>, Error> {
        match self {
            Expr::Nil => Ok(None),
            this => this.eval(system_data).map(Some),
        }
    }
}

impl Eval<msg::Target> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<msg::Target, Error> {
        match self {
            Expr::Nil => Ok(msg::Target::Nil),
            Expr::Target(target) => target.eval(system_data),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<()> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<(), Error> {
        match self {
            Expr::Nil => Ok(()),
            Expr::Query(msg) => msg.eval(system_data),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<String> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<String, Error> {
        match self {
            Expr::Atom(string) => Ok(string.clone()),
            Expr::String(string) => Ok(string.clone()),
            Expr::Query(msg) => msg.eval(system_data),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<f32> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<f32, Error> {
        match self {
            Expr::Real(f) => Ok(*f),
            Expr::Query(msg) => msg.eval(system_data),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<bool> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<bool, Error> {
        match self {
            Expr::Bool(p) => Ok(*p),
            Expr::Query(msg) => msg.eval(system_data),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<i64> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<i64, Error> {
        match self {
            Expr::Int(i) => Ok(*i),
            Expr::Query(msg) => msg.eval(system_data),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<i32> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<i32, Error> {
        Ok(Eval::<i64>::eval(self, system_data)? as i32)
    }
}

impl Eval<usize> for Expr {
    fn eval(&self, system_data: SystemData) -> Result<usize, Error> {
        Ok(Eval::<i64>::eval(self, system_data)? as usize)
    }
}

impl Eval<msg::Target> for Target {
    fn eval(&self, system_data: SystemData) -> Result<msg::Target, Error> {
        match self {
            Target::Game => Ok(msg::Target::Game),
            Target::Room(room) => Ok(msg::Target::Room(room.eval(system_data)?)),
            Target::Entities(name) => Ok(msg::Target::Entities(name.eval(system_data)?)),
            Target::Entity(name, idx) => Ok(msg::Target::Entity(name.eval(system_data)?, idx.eval(system_data)?)),
        }
    }
}

impl Eval<Option<msg::Message>> for Message {
    fn eval(&self, system_data: SystemData) -> Result<Option<msg::Message>, Error> {
        let target = self.target.eval(system_data)?;
        let (_, _, _, _, scripts, _, _) = system_data;
        let cmd = match &self.cmd {
            Command::Bind(value, lambda) => {
                let value = value.eval(system_data).unwrap();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &[value])
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::BindList(values, lambda) => {
                let values = values
                    .iter()
                    .map(|value| value.eval(system_data).unwrap())
                    .collect::<Vec<_>>();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &values)
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::Quit(status, msg) => Some(msg::Command::Quit(status.eval(system_data)?, msg.eval(system_data)?)),
            Command::LoadRoom(room) => Some(msg::Command::LoadRoom(room.eval(system_data)?)),
            Command::Save(save_file) => Some(msg::Command::Save(save_file.eval(system_data)?)),
            Command::Load(save_file) => Some(msg::Command::Load(save_file.eval(system_data)?)),
            Command::CreateEntity(name) => Some(msg::Command::CreateEntity(name.eval(system_data)?)),
            Command::DeleteEntity => Some(msg::Command::DeleteEntity),
            Command::AddForce(x, y, z) => {
                Some(msg::Command::AddForce(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                ))
            }
            Command::TranslateBy(x, y, z) => {
                Some(msg::Command::TranslateBy(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                ))
            }
            Command::RotateBy(x, y, z, w) => {
                Some(msg::Command::RotateBy(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                    w.eval(system_data)?,
                ))
            }
            Command::ScaleBy(x, y, z) => {
                Some(msg::Command::ScaleBy(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                ))
            }
            Command::SetPosition(x, y, z) => {
                Some(msg::Command::SetPosition(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                ))
            }
            Command::SetRotation(x, y, z, w) => {
                Some(msg::Command::SetRotation(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                    w.eval(system_data)?,
                ))
            }
            Command::SetScale(x, y, z) => {
                Some(msg::Command::SetScale(
                    x.eval(system_data)?,
                    y.eval(system_data)?,
                    z.eval(system_data)?,
                ))
            }
            Command::AddItem(name) => Some(msg::Command::AddItem(name.eval(system_data)?)),
            Command::RemoveItem(name) => Some(msg::Command::RemoveItem(name.eval(system_data)?)),
            Command::Nil
            | Command::CurrentRoom
            | Command::IsKind(..)
            | Command::IsAlive
            | Command::HasItem(..)
            | Command::PositionX
            | Command::PositionY
            | Command::PositionZ
            | Command::RotationX
            | Command::RotationY
            | Command::RotationZ
            | Command::RotationW
            | Command::ScaleX
            | Command::ScaleY
            | Command::ScaleZ => None,
        };
        Ok(cmd.map(|cmd| msg::Message::new(target, cmd)))
    }
}

impl Eval<()> for Message {
    fn eval(&self, system_data: SystemData) -> Result<(), Error> {
        let (_, _, _, _, scripts, _, _) = system_data;
        match self.cmd {
            Command::Bind(ref value, ref lambda) => {
                let value = value.eval(system_data).unwrap();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &[value])
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::BindList(ref values, ref lambda) => {
                let values = values
                    .iter()
                    .map(|value| value.eval(system_data).unwrap())
                    .collect::<Vec<_>>();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &values)
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::Quit(..)
            | Command::LoadRoom(..)
            | Command::Save(..)
            | Command::Load(..)
            | Command::CreateEntity(..)
            | Command::DeleteEntity
            | Command::AddForce(..)
            | Command::TranslateBy(..)
            | Command::RotateBy(..)
            | Command::ScaleBy(..)
            | Command::SetPosition(..)
            | Command::SetRotation(..)
            | Command::SetScale(..)
            | Command::AddItem(..)
            | Command::RemoveItem(..)
            | Command::Nil => Ok(()),
            Command::CurrentRoom
            | Command::IsKind(..)
            | Command::IsAlive
            | Command::HasItem(..)
            | Command::PositionX
            | Command::PositionY
            | Command::PositionZ
            | Command::RotationX
            | Command::RotationY
            | Command::RotationZ
            | Command::RotationW
            | Command::ScaleX
            | Command::ScaleY
            | Command::ScaleZ => Err(Error::InvalidType),
        }
    }
}

impl Eval<String> for Message {
    fn eval(&self, system_data: SystemData) -> Result<String, Error> {
        match self.target.eval(system_data)? {
            msg::Target::Game => {}
            _ => return Err(Error::InvalidTarget),
        }
        let (_, _, _, _, _, current_room, _) = system_data;
        match self.cmd {
            Command::CurrentRoom => Ok(current_room.room.clone()),
            _ => Err(Error::InvalidType),
        }
    }
}

impl Eval<i64> for Message {
    fn eval(&self, _system_data: SystemData) -> Result<i64, Error> {
        Err(Error::InvalidType)
    }
}

impl Eval<i32> for Message {
    fn eval(&self, _system_data: SystemData) -> Result<i32, Error> {
        Err(Error::InvalidType)
    }
}

impl Eval<bool> for Message {
    fn eval(&self, system_data: SystemData) -> Result<bool, Error> {
        let target = match self.target.eval(system_data)? {
            msg::Target::Entity(name, idx) => Name::new(name, idx),
            _ => return Err(Error::InvalidTarget),
        };
        let (names, inv, _, _, scripts, _, entities) = system_data;
        match self.cmd {
            Command::Bind(ref value, ref lambda) => {
                let value = value.eval(system_data).unwrap();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &[value])
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::BindList(ref values, ref lambda) => {
                let values = values
                    .iter()
                    .map(|value| value.eval(system_data).unwrap())
                    .collect::<Vec<_>>();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &values)
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::Quit(..)
            | Command::LoadRoom(..)
            | Command::Save(..)
            | Command::Load(..)
            | Command::CreateEntity(..)
            | Command::DeleteEntity
            | Command::AddForce(..)
            | Command::TranslateBy(..)
            | Command::RotateBy(..)
            | Command::ScaleBy(..)
            | Command::SetPosition(..)
            | Command::SetRotation(..)
            | Command::SetScale(..)
            | Command::AddItem(..)
            | Command::RemoveItem(..)
            | Command::CurrentRoom
            | Command::PositionX
            | Command::PositionY
            | Command::PositionZ
            | Command::RotationX
            | Command::RotationY
            | Command::RotationZ
            | Command::RotationW
            | Command::ScaleX
            | Command::ScaleY
            | Command::ScaleZ
            | Command::Nil => Err(Error::InvalidType),
            Command::IsKind(ref kind) => {
                let kind: String = kind.eval(system_data)?;
                Ok(target.name() == kind)
            }
            Command::IsAlive => Ok((names, entities).join().any(|(name, _)| name == &target)),
            Command::HasItem(ref item) => {
                let item: String = item.eval(system_data)?;
                Ok((names, inv)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, inv)| inv.contains_by_name(&item).is_some())
                    .unwrap_or(false))
            }
        }
    }
}

impl Eval<f32> for Message {
    fn eval(&self, system_data: SystemData) -> Result<f32, Error> {
        let target = match self.target.eval(system_data)? {
            msg::Target::Entity(name, idx) => Name::new(name, idx),
            _ => return Err(Error::InvalidTarget),
        };
        let (names, _, poses, transforms, scripts, _, _) = system_data;
        match self.cmd {
            Command::Bind(ref value, ref lambda) => {
                let value = value.eval(system_data).unwrap();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &[value])
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::BindList(ref values, ref lambda) => {
                let values = values
                    .iter()
                    .map(|value| value.eval(system_data).unwrap())
                    .collect::<Vec<_>>();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &values)
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::Quit(..)
            | Command::LoadRoom(..)
            | Command::Save(..)
            | Command::Load(..)
            | Command::CreateEntity(..)
            | Command::DeleteEntity
            | Command::AddForce(..)
            | Command::TranslateBy(..)
            | Command::RotateBy(..)
            | Command::ScaleBy(..)
            | Command::SetPosition(..)
            | Command::SetRotation(..)
            | Command::SetScale(..)
            | Command::AddItem(..)
            | Command::RemoveItem(..)
            | Command::Nil
            | Command::CurrentRoom
            | Command::IsKind(..)
            | Command::IsAlive
            | Command::HasItem(..) => Err(Error::InvalidType),
            Command::PositionX => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.position().x)
                    .ok_or(Error::InvalidEntity)
            }
            Command::PositionY => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.position().y)
                    .ok_or(Error::InvalidEntity)
            }
            Command::PositionZ => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.position().z)
                    .ok_or(Error::InvalidEntity)
            }
            Command::RotationX => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.rotation().v.x)
                    .ok_or(Error::InvalidEntity)
            }
            Command::RotationY => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.rotation().v.y)
                    .ok_or(Error::InvalidEntity)
            }
            Command::RotationZ => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.rotation().v.z)
                    .ok_or(Error::InvalidEntity)
            }
            Command::RotationW => {
                (names, poses)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, pose)| pose.rotation().s)
                    .ok_or(Error::InvalidEntity)
            }
            Command::ScaleX => {
                (names, transforms)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, transform)| transform.scale().x)
                    .ok_or(Error::InvalidEntity)
            }
            Command::ScaleY => {
                (names, transforms)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, transform)| transform.scale().y)
                    .ok_or(Error::InvalidEntity)
            }
            Command::ScaleZ => {
                (names, transforms)
                    .join()
                    .filter(|(name, _)| name == &&target)
                    .next()
                    .map(|(_, transform)| transform.scale().z)
                    .ok_or(Error::InvalidEntity)
            }
        }
    }
}

impl Eval<Value> for Message {
    fn eval(&self, system_data: SystemData) -> Result<Value, Error> {
        let (names, inventories, poses, transforms, scripts, current_room, entities) = system_data;
        match self.cmd {
            Command::Bind(ref value, ref lambda) => {
                let value = value.eval(system_data).unwrap();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &[value])
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::BindList(ref values, ref lambda) => {
                let values = values
                    .iter()
                    .map(|value| value.eval(system_data).unwrap())
                    .collect::<Vec<_>>();
                let msg = {
                    let mut interp = scripts.interp().lock().expect("lispi was poisoned");
                    interp
                        .call_anonymous(Lambda::clone(&*lambda), &values)
                        .expect("bind failed")
                };
                match msg.kind() {
                    ValueKind::Any(any) => {
                        let expr = Arc::clone(any);
                        match expr.downcast_ref::<Message>() {
                            Some(expr) => return expr.eval(system_data),
                            None => panic!("invalid expression"),
                        }
                    }
                    _ => panic!("invalid expression"),
                }
            }
            Command::Quit(..)
            | Command::LoadRoom(..)
            | Command::Save(..)
            | Command::Load(..)
            | Command::CreateEntity(..)
            | Command::DeleteEntity
            | Command::AddForce(..)
            | Command::TranslateBy(..)
            | Command::RotateBy(..)
            | Command::ScaleBy(..)
            | Command::SetPosition(..)
            | Command::SetRotation(..)
            | Command::SetScale(..)
            | Command::AddItem(..)
            | Command::RemoveItem(..)
            | Command::Nil => Ok(Value::from(LinkedList::new())),
            Command::CurrentRoom => Ok(Value::from(RcStr::from(current_room.room.clone()))),
            Command::IsKind(ref kind) => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                let kind: String = kind.eval(system_data)?;
                Ok(Value::from(target.name() == kind))
            }
            Command::IsAlive => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from((names, entities).join().any(|(name, _)| name == &target)))
            }
            Command::HasItem(ref item) => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                let item: String = item.eval(system_data)?;
                Ok(Value::from(
                    (names, inventories)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, inv)| inv.contains_by_name(&item).is_some())
                        .unwrap_or(false),
                ))
            }
            Command::PositionX => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.position().x)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::PositionY => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.position().y)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::PositionZ => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.position().z)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::RotationX => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.rotation().v.x)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::RotationY => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.rotation().v.y)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::RotationZ => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.rotation().v.z)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::RotationW => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, poses)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, pose)| pose.rotation().s)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::ScaleX => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, transforms)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, transform)| transform.scale().x)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::ScaleY => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, transforms)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, transform)| transform.scale().y)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
            Command::ScaleZ => {
                let target = match self.target.eval(system_data)? {
                    msg::Target::Entity(name, idx) => Name::new(name, idx),
                    _ => return Err(Error::InvalidTarget),
                };
                Ok(Value::from(
                    (names, transforms)
                        .join()
                        .filter(|(name, _)| name == &&target)
                        .next()
                        .map(|(_, transform)| transform.scale().z)
                        .ok_or(Error::InvalidEntity)? as f64,
                ))
            }
        }
    }
}
