use std::any::Any;
use std::sync::{Arc, Mutex};
use std::convert::TryFrom;

use amethyst::shrev::EventChannel;

use lispi::error::{Error as LispiError, ErrorKind as LispiErrorKind};
use lispi::collections::LinkedList;
use lispi::expr::{Value, ValueKind, Lambda, Ffi};

use crate::scripting::raw::{self, Expr};

#[derive(Debug, Clone, Copy)]
pub struct Game;

impl Ffi for Game {
    fn display_name(&self) -> &str {
        "game"
    }

    fn argc(&self) -> usize {
        0
    }

    fn call(&self, _args: &[Value]) -> Result<Value, LispiError> {
        let target = Expr::Target(Arc::new(raw::Target::Game));
        Ok(Value::from(Arc::new(target) as Arc<Any + Send + Sync>))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Room;

impl Ffi for Room {
    fn display_name(&self) -> &str {
        "room"
    }

    fn argc(&self) -> usize {
        1
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let arg = &args[0];
        let expr = atom_expr(arg);
        let target = Expr::Target(Arc::new(raw::Target::Room(expr)));
        Ok(Value::from(Arc::new(target) as Arc<Any + Send + Sync>))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct FindEntity;

impl Ffi for FindEntity {
    fn display_name(&self) -> &str {
        "find-entity"
    }

    fn argc(&self) -> usize {
        2
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let arg0 = &args[0];
        let arg1 = &args[1];
        let name = atom_expr(arg0);
        let idx = int_expr(arg1);
        let target = Expr::Target(Arc::new(raw::Target::Entity(name, idx)));
        Ok(Value::from(Arc::new(target) as Arc<Any + Send + Sync>))
    }
}

#[derive(Debug, Clone, Copy)]
pub struct FindEntities;

impl Ffi for FindEntities {
    fn display_name(&self) -> &str {
        "find-entities"
    }

    fn argc(&self) -> usize {
        1
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let arg0 = &args[0];
        let name = atom_expr(arg0);
        let target = Expr::Target(Arc::new(raw::Target::Entities(name)));
        Ok(Value::from(Arc::new(target) as Arc<Any + Send + Sync>))
    }
}

#[derive(Default)]
pub struct SendMessage {
    channel: Arc<Mutex<EventChannel<raw::Message>>>,
}

impl SendMessage {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn channel(&self) -> Arc<Mutex<EventChannel<raw::Message>>> {
        Arc::clone(&self.channel)
    }
}

impl Ffi for SendMessage {
    fn display_name(&self) -> &str {
        "send-message"
    }

    fn argc(&self) -> usize {
        2
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let (message, ret) = message(args)?;
        if let Some(message) = message {
            self.channel
                .lock()
                .expect("raw message channel was poisoned")
                .single_write(message);
        }
        Ok(ret)
    }
}

#[derive(Default)]
pub struct Message;

impl Ffi for Message {
    fn display_name(&self) -> &str {
        "message"
    }

    fn argc(&self) -> usize {
        2
    }

    fn call(&self, args: &[Value]) -> Result<Value, LispiError> {
        let (message, _) = message(args)?;
        match message {
            Some(message) => Ok(Value::from(Arc::new(message) as Arc<dyn Any + Send + Sync>)),
            None => {
                Ok(Value::from(Arc::new(raw::Message {
                    target: raw::Expr::Nil,
                    cmd: raw::Command::Nil,
                }) as Arc<dyn Any + Send + Sync>))
            }
        }
    }
}

fn message(args: &[Value]) -> Result<(Option<raw::Message>, Value), LispiError> {
    let arg0 = &args[0];
    let arg1 = &args[1];
    let target = expr(arg0);
    let cmd = LinkedList::try_from(arg1.clone())?;
    let kind = match cmd.front().expect("no command passed").kind() {
        ValueKind::Atom(atom) => atom.clone(),
        _ => panic!("invalid command"),
    };
    let args = cmd.pop_front().0;
    match kind.as_str() {
        "nothing" => Ok((None, Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>))),
        "bind" => {
            let value = args.front().expect("missing argument in command");
            let value = expr(value);
            let lambda = args.nth(1).expect("missing argument in command");
            let lambda =
                Arc::<Lambda>::try_from(lambda).expect("the second argument in a bind command must be a closure");
            let cmd = raw::Command::Bind(value, lambda);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message.clone()),
                Value::from(Arc::new(Expr::Query(Arc::new(message))) as Arc<Any + Send + Sync>),
            ))
        }
        "bind-list" => {
            let value = args.front().expect("missing argument in command");
            let values = match value.kind() {
                ValueKind::List(list) => list.list().iter().map(expr).collect(),
                _ => {
                    return Err(LispiError::new(LispiErrorKind::InvalidType)
                        .span(value.span())
                        .describe("the first argument to `bind-list` must be a list"));
                }
            };
            let lambda = args.nth(1).expect("missing argument in command");
            let lambda =
                Arc::<Lambda>::try_from(lambda).expect("the second argument in a bind command must be a closure");
            let cmd = raw::Command::BindList(values, lambda);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message.clone()),
                Value::from(Arc::new(Expr::Query(Arc::new(message))) as Arc<Any + Send + Sync>),
            ))
        }
        "quit" => {
            let status = args.front().map(|status| int_expr(status)).unwrap_or_default();
            let msg = args.back().map(|msg| string_expr(msg)).unwrap_or_default();
            let cmd = raw::Command::Quit(status, msg);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "load-room" => {
            let name = args.front().expect("missing argument in command");
            let name = string_expr(name);
            let cmd = raw::Command::LoadRoom(name);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "save" => {
            let name = args.front().expect("missing argument in command");
            let name = string_expr(name);
            let cmd = raw::Command::Load(name);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "load" => {
            let name = args.front().expect("missing argument in command");
            let name = string_expr(name);
            let cmd = raw::Command::Save(name);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "create-entity" => {
            let entity = args.front().expect("missing argument in command");
            let entity = atom_expr(entity);
            let cmd = raw::Command::CreateEntity(entity);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "delete-entity" => {
            let cmd = raw::Command::DeleteEntity;
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "add-force" => {
            let accel = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&accel.nth(0).expect("missing x position"));
            let y = real_expr(&accel.nth(1).expect("missing y position"));
            let z = real_expr(&accel.nth(2).expect("missing z position"));
            let cmd = raw::Command::AddForce(x, y, z);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "translate-by" => {
            let position = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&position.nth(0).expect("missing x position"));
            let y = real_expr(&position.nth(1).expect("missing y position"));
            let z = real_expr(&position.nth(2).expect("missing z position"));
            let cmd = raw::Command::TranslateBy(x, y, z);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "rotate-by" => {
            let rotation = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&rotation.nth(0).expect("missing x rotation"));
            let y = real_expr(&rotation.nth(1).expect("missing z rotation"));
            let z = real_expr(&rotation.nth(2).expect("missing z rotation"));
            let w = real_expr(&rotation.nth(3).expect("missing w rotation"));
            let cmd = raw::Command::RotateBy(x, y, z, w);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "scale-by" => {
            let scale = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&scale.nth(0).expect("missing x scale"));
            let y = real_expr(&scale.nth(1).expect("missing y scale"));
            let z = real_expr(&scale.nth(2).expect("missing z scale"));
            let cmd = raw::Command::ScaleBy(x, y, z);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "set-position" => {
            let position = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&position.nth(0).expect("missing x position"));
            let y = real_expr(&position.nth(1).expect("missing y position"));
            let z = real_expr(&position.nth(2).expect("missing z position"));
            let cmd = raw::Command::SetPosition(x, y, z);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "set-rotation" => {
            let rotation = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&rotation.nth(0).expect("missing x rotation"));
            let y = real_expr(&rotation.nth(1).expect("missing y rotation"));
            let z = real_expr(&rotation.nth(2).expect("missing z rotation"));
            let w = real_expr(&rotation.nth(3).expect("missing w rotation"));
            let cmd = raw::Command::SetRotation(x, y, z, w);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "set-scale" => {
            let scale = LinkedList::try_from(args.front().expect("missing argument in command").clone())?;
            let x = real_expr(&scale.nth(0).expect("missing x scale"));
            let y = real_expr(&scale.nth(1).expect("missing y scale"));
            let z = real_expr(&scale.nth(2).expect("missing z scale"));
            let cmd = raw::Command::SetScale(x, y, z);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "add-item" => {
            let item = args.front().expect("missing argument in command");
            let item = atom_expr(item);
            let cmd = raw::Command::AddItem(item);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "remove-item" => {
            let item = args.front().expect("missing argument in command");
            let item = atom_expr(item);
            let cmd = raw::Command::RemoveItem(item);
            let message = raw::Message { target, cmd };
            Ok((
                Some(message),
                Value::from(Arc::new(Expr::Nil) as Arc<Any + Send + Sync>),
            ))
        }
        "current-room" => {
            let cmd = raw::Command::CurrentRoom;
            let message = raw::Message { target, cmd };
            let message = Expr::Query(Arc::new(message));
            Ok((None, Value::from(Arc::new(message) as Arc<Any + Send + Sync>)))
        }
        "kind-eq?" => {
            let kind = atom_expr(args.front().expect("missing argument in command"));
            let cmd = raw::Command::IsKind(kind);
            let message = raw::Message { target, cmd };
            let message = Expr::Query(Arc::new(message));
            Ok((None, Value::from(Arc::new(message) as Arc<Any + Send + Sync>)))
        }
        "alive?" => {
            let cmd = raw::Command::IsAlive;
            let message = raw::Message { target, cmd };
            let message = Expr::Query(Arc::new(message));
            Ok((None, Value::from(Arc::new(message) as Arc<Any + Send + Sync>)))
        }
        "has-item?" => {
            let item = atom_expr(args.front().expect("missing argument in command"));
            let cmd = raw::Command::HasItem(item);
            let message = raw::Message { target, cmd };
            let message = Expr::Query(Arc::new(message));
            Ok((None, Value::from(Arc::new(message) as Arc<Any + Send + Sync>)))
        }
        "position" => {
            let position_x = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::PositionX,
            }));
            let position_y = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::PositionY,
            }));
            let position_z = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::PositionZ,
            }));
            let position = LinkedList::from(vec![
                Value::from(Arc::new(position_x) as Arc<dyn Any + Send + Sync>),
                Value::from(Arc::new(position_y) as Arc<dyn Any + Send + Sync>),
                Value::from(Arc::new(position_z) as Arc<dyn Any + Send + Sync>),
            ]);
            Ok((None, Value::from(position)))
        }
        "rotation" => {
            let rotation_x = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::RotationX,
            }));
            let rotation_y = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::RotationY,
            }));
            let rotation_z = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::RotationZ,
            }));
            let rotation = LinkedList::from(vec![
                Value::from(Arc::new(rotation_x) as Arc<dyn Any + Send + Sync>),
                Value::from(Arc::new(rotation_y) as Arc<dyn Any + Send + Sync>),
                Value::from(Arc::new(rotation_z) as Arc<dyn Any + Send + Sync>),
            ]);
            Ok((None, Value::from(rotation)))
        }
        "scale" => {
            let scale_x = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::ScaleX,
            }));
            let scale_y = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::ScaleY,
            }));
            let scale_z = Expr::Query(Arc::new(raw::Message {
                target: target.clone(),
                cmd: raw::Command::ScaleZ,
            }));
            let scale = LinkedList::from(vec![
                Value::from(Arc::new(scale_x) as Arc<dyn Any + Send + Sync>),
                Value::from(Arc::new(scale_y) as Arc<dyn Any + Send + Sync>),
                Value::from(Arc::new(scale_z) as Arc<dyn Any + Send + Sync>),
            ]);
            Ok((None, Value::from(scale)))
        }
        x => panic!("invalid command: {}", x),
    }
}

fn expr(value: &Value) -> Expr {
    match value.kind() {
        ValueKind::Any(any) => {
            let expr = Arc::clone(any);
            match expr.downcast_ref::<Expr>() {
                Some(expr) => expr.clone(),
                None => panic!("invalid expression"),
            }
        }
        _ => panic!("invalid expression"),
    }
}

fn int_expr(value: &Value) -> Expr {
    match value.kind() {
        ValueKind::Int(int) => Expr::Int(*int),
        ValueKind::Any(any) => {
            let expr = Arc::clone(any);
            match expr.downcast_ref::<Expr>() {
                Some(expr) => expr.clone(),
                None => panic!("invalid expression"),
            }
        }
        _ => panic!("invalid expression"),
    }
}

fn real_expr(value: &Value) -> Expr {
    match value.kind() {
        ValueKind::Float(real) => Expr::Real(*real as _),
        ValueKind::Any(any) => {
            let expr = Arc::clone(any);
            match expr.downcast_ref::<Expr>() {
                Some(expr) => expr.clone(),
                None => panic!("invalid expression"),
            }
        }
        _ => panic!("invalid expression"),
    }
}

fn atom_expr(value: &Value) -> Expr {
    match value.kind() {
        ValueKind::Atom(atom) => Expr::Atom(atom.to_string()),
        ValueKind::Any(any) => {
            let expr = Arc::clone(any);
            match expr.downcast_ref::<Expr>() {
                Some(expr) => expr.clone(),
                None => panic!("invalid expression"),
            }
        }
        _ => panic!("invalid expression"),
    }
}

fn string_expr(value: &Value) -> Expr {
    match value.kind() {
        ValueKind::String(atom) => Expr::String(atom.to_string()),
        ValueKind::Any(any) => {
            let expr = Arc::clone(any);
            match expr.downcast_ref::<Expr>() {
                Some(expr) => expr.clone(),
                None => panic!("invalid expression"),
            }
        }
        _ => panic!("invalid expression"),
    }
}
