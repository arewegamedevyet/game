# Are we gamedev yet?

Almost!  This project explores Rust in 2018 as a gamedev language, together with
Amethyst as the engine of choice and various other tools, like git (obviously),
cargo (obviously), blender, krita, ...

Every piece of code will be written during one of the streams over at
[twitch.tv/walterpi](https://twitch.tv/walterpi) and archived on
[YouTube](https://www.youtube.com/channel/UCyKF9FafoRauY2F05eE1ZwQ).

The source code is licensed with a [permissive, free license](./LICENSE).