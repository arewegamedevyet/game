# Message api

## Sending messages

Sending a `quit` message.
```scheme
(send-message (game) '(quit))
```

## Receiving messages
Ask what the current room is
```scheme
(define room (send-message (game) '(query current-room)))
```

## Finding entities
Move the "player{0}" entity by one unit up.
```scheme
(define player (find-entity 'player 0))
(send-message player '(translate (0.0 1.0 0.0)))
```

Find all ghosts and kill them
```scheme
(define ghosts (find-entities 'ghost))
(send-message ghosts '(kill))
```

## Game messages
#### Quit
```scheme
'(quit)
'(quit int)
'(quit int string)
```

#### Load room
```scheme
'(load-room string)
```

### Save/load game
```scheme
'(save string)
'(load string)
```

### Controlling entities
```scheme
'(create-entity atom) ; returns Target::Entity
'(delete-entity target)
```

## Entity messages
### Commands
```scheme
'(translate-by (real real real))
'(rotate-by (real real real))
'(scale-by (real real real))
'(set-position (real real real))
'(set-rotation (real real real real))
'(set-scale (real real real))
'(add-item atom)
'(remove-item atom)
```

Queries
```scheme
'(alive?) ; returns bool
'(position) ; returns (real real real)
'(rotation) ; returns (real real real real)
'(scale) ; returns (real real real)
'(has-item? atom) ; returns bool
```
